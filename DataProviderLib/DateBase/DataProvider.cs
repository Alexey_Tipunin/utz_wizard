﻿using PetaPoco;

namespace DataProviderLib.DateBase
{
    public class DataProvider
    {
        private Database _db;

        public DataProvider()
        {
            Connect();
        }

        private void Connect()
        {
            Db = new Database(ConnectionString, Provider);
        }

        public static string ConnectionString
        {
            get
            {
                return string.Format(@"Data Source={2};Database=training;user id={0};password={1};port=3306;Connection Timeout=30;", DbAsername, DbPassword, IPAddress);
            }
        }

        public static string Provider
        {
            get
            {
                return "MySql.Data.MySqlClient";
            }
        }
        

        private static string _ipAddress;
        private static string _dbPassword;
        private static string _dbAsername;

        public static string IPAddress
        {
            get { return _ipAddress ?? "10.0.0.5"; }
            set { _ipAddress = value; }
        }

        public static string DbPassword
        {
            get { return _dbPassword ?? "psgrandpass"; }
            set { _dbPassword = value; }
        }

        public static string DbAsername
        {
            get { return _dbAsername ?? "root"; }
            set { _dbAsername = value; }
        }

        public Database Db
        {
            get
            {
                return _db;
            }

            set
            {
                _db = value;
            }
        }
    }
}
