﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Editor
{
    public struct s_Properties
    {
        public string name;
        public string value;
        public string reg_MB;
        public Type type;
        public object teg;

    }

    public interface DrawObj
    {
        int x1 { get; set; }
        int x2 { get; set; }
        int y1 { get; set; }
        int y2 { get; set; }
        string NAME { get; set; }
        int points { get; set; }
        int set_point { get; set; }
        void SetPoint(int X, int Y);
        bool OnClick(int X, int Y);
        void OnPaint(Graphics g);
        string toSave();
        List<s_Properties> getProperties();
    }
}
