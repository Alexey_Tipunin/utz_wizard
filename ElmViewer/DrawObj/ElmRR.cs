﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Editor
{
    public class ElmRR : DrawObj
    {
        public static string Name = "PP";
        public static Point clickPoint;
        private bool isSelect;
        //private int set_point;
        public int num;
        private const int GRID_SIZE = 16;
        private static int Count;
        //public int x1, x2, y1, y2;
        public int type;
        //public int point;

        public ElmRR()
        {
            Count++;
            num = Count;
            NAME = Name;
        }

        public void OnPaint(Graphics g)
        {
            Point[] zdPoint = new Point[7];

            int _x = Math.Abs(x1 - x2) / 2 + Math.Min(x1, x2);
            int _y = Math.Abs(y1 - y2) / 2 + Math.Min(y1, y2);
            string s = NAME;
            SizeF s_size = g.MeasureString(s, new Font("Arial", 8, FontStyle.Bold));
            g.DrawLine(new Pen(Color.RoyalBlue, 4), new Point(x1, y1), new Point(x2, y2));
            //======================================================
            float A = (float)(Math.Atan2(y1 - y2, x1 - x2) / Math.PI * 180);
            if (A >= 90) A -= 180;
            if (A <= -90) A += 180;
            g.TranslateTransform(_x, _y);
            g.RotateTransform(A);
            g.DrawString(s, new Font("Arial", 8, FontStyle.Bold), new SolidBrush(Color.Black), -s_size.Width / 2, -GRID_SIZE * 2);
            g.FillEllipse(new SolidBrush(Color.Gainsboro), -GRID_SIZE, -GRID_SIZE, GRID_SIZE * 2, GRID_SIZE * 2);
            g.DrawEllipse(new Pen(Color.RoyalBlue, 3), -GRID_SIZE, -GRID_SIZE, GRID_SIZE * 2, GRID_SIZE * 2);
            g.DrawEllipse(new Pen(Color.RoyalBlue, 3), -1, -1, 2, 2);
            g.DrawLine(new Pen(Color.RoyalBlue, 3), 0, -GRID_SIZE+4, 0, GRID_SIZE-4);
            //g.FillPolygon(new SolidBrush(Color.GreenYellow), zdPoint);
            //g.DrawPolygon(new Pen(Color.RoyalBlue, 2), zdPoint);
            g.RotateTransform(-A);
            g.TranslateTransform(-_x, -_y);
            //=======================================================

            if (isSelect)
            {
                if (points == 1)
                    g.DrawRectangle(new Pen(Color.Red, 2), x1 - GRID_SIZE / 4, y1 - GRID_SIZE / 4, GRID_SIZE / 2, GRID_SIZE / 2);
                else
                    g.DrawRectangle(new Pen(Color.RoyalBlue, 2), x1 - GRID_SIZE / 4, y1 - GRID_SIZE / 4, GRID_SIZE / 2, GRID_SIZE / 2);
                if (points == 2)
                    g.DrawRectangle(new Pen(Color.Red, 2), x2 - GRID_SIZE / 4, y2 - GRID_SIZE / 4, GRID_SIZE / 2, GRID_SIZE / 2);
                else
                    g.DrawRectangle(new Pen(Color.RoyalBlue, 2), x2 - GRID_SIZE / 4, y2 - GRID_SIZE / 4, GRID_SIZE / 2, GRID_SIZE / 2);
            }
        }

        public string toSave()
        {
            return "r\t" + x1 + "\t" + y1 + "\t" + x2 + "\t" + y2 + "\t" + NAME;
        }

        public bool OnClick(int X, int Y)
        {
            var dx = (x1 - x2);
            var dy = (y1 - y2);
            var dLengh = Math.Sqrt(dx * dx + dy * dy);
            dx = (x1 - X);
            dy = (y1 - Y);
            var dLengh1 = Math.Sqrt(dx * dx + dy * dy);
            dx = (x2 - X);
            dy = (y2 - Y);
            var dLengh2 = Math.Sqrt(dx * dx + dy * dy);
            isSelect = Math.Abs(dLengh - (dLengh1 + dLengh2)) < 0.2;
            if (isSelect)
                if (Math.Abs(dLengh1) < 10)
                    points = 1;
                else if (Math.Abs(dLengh2) < 10)
                    points = 2;
                else
                    points = -1;
            else
                points = 0;
            return isSelect;
        }

        public void SetPoint(int X, int Y)
        {
            if (set_point == 0)
            {
                x1 = X;
                y1 = Y;
                set_point++;
            }
            else if (set_point == 1)
            {
                x2 = X;
                y2 = Y;
                set_point++;
            }
        }
        //====
        public int x1 { get; set; }

        public int x2 { get; set; }

        public int y1 { get; set; }

        public int y2 { get; set; }

        public string NAME { get; set; }

        public int points { get; set; }

        public int set_point { get; set; }

        public List<s_Properties> getProperties()
        {
            List<s_Properties> pr = new List<s_Properties>();
            s_Properties item = new s_Properties { name = "Tag name", value = Name };
            pr.Add(item);
            return pr;
        }
    }
}