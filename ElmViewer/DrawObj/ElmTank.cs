﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Editor
{
    public class ElmTank : DrawObj
    {
        public static string Name = "Tank";
        public static Point clickPoint;
        private bool isSelect;
        //private int set_point;
        public int num;
        private const int GRID_SIZE = 16;
        private static int Count;
        //public int x1, x2, y1, y2;
        public int type;
        private int _x1;
        private int _y1;

        //Свойства объекта


        public float plot { get; set; }     // Плотность жидкости *)
        public bool k_v { get; set; }//(* Кинематическая вязкасть *)
        public bool t { get; set; }//(* Температура жидкости *)
        public bool P { get; set; }//


        //Свойства объекта
        public ElmTank()
        {
            Count++;
            num = Count;
            NAME = Name;
        }

        public void OnPaint(Graphics g)
        {
            string s = NAME;
            g.FillRectangle(new SolidBrush(Color.DodgerBlue), x1 - GRID_SIZE * 2, y1 - GRID_SIZE * 2, GRID_SIZE * 4, GRID_SIZE * 3);
            g.DrawRectangle(new Pen(Color.RoyalBlue, 3), x1 - GRID_SIZE * 2, y1 - GRID_SIZE * 4, GRID_SIZE * 4, GRID_SIZE * 5);
            g.DrawString(s, new Font("Arial", 8, FontStyle.Bold), new SolidBrush(Color.Black), x1 - GRID_SIZE * 2, y1 - GRID_SIZE * 5);

            //g.DrawLine(new Pen(Color.RoyalBlue, 4), new Point(x1, y1), new Point(x2, y2));
            if (isSelect)
            {
                g.DrawRectangle(new Pen(Color.Red, 3), x1 - GRID_SIZE * 2, y1 - GRID_SIZE * 4, GRID_SIZE * 4, GRID_SIZE * 5);
            }
        }

        public string toSave()
        {
            return "t\t" + x1 + "\t" + y1 + "\t" + x2 + "\t" + y2 + "\t" + NAME;
            //throw new NotImplementedException();
        }

        public bool OnClick(int X, int Y)
        {
            var _x1 = x1 - GRID_SIZE * 2;
            var _y1 = y1 - GRID_SIZE * 4;
            var _x2 = x1 + GRID_SIZE * 2;
            var _y2 = y1 + GRID_SIZE;
            if ((_x1 < X) && (_y1 < Y) && (_x2 > X) && (_y2 > Y))
                isSelect = true;
            else
                isSelect = false;
            if (isSelect)
                points = 1;
            else
                points = 0;
            return isSelect;
        }

        public void SetPoint(int X, int Y)
        {
            x1 = X;
            y1 = Y;
            x2 = X;
            y2 = Y;
            //set_point++;
        }

        //====
        public int x1
        {
            get { return _x1; }
            set { _x1 = value; }
        }

        public int x2
        {
            get { return _x1; }
            set { _x1 = value; }
        }

        public int y1
        {
            get { return _y1; }
            set { _y1 = value; }
        }

        public int y2
        {
            get { return _y1; }
            set { _y1 = value; }
        }

        public string NAME { get; set; }

        public int points { get; set; }

        public int set_point { get; set; }

        public List<s_Properties> getProperties()
        {
            List<s_Properties> pr = new List<s_Properties>();
            s_Properties item = new s_Properties { name = "Tag name", value = Name };
            pr.Add(item);
            return pr;
        }
    }
}
