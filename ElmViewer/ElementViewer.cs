﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Svg;

namespace ElmViewer
{
    public partial class ElementViewer : UserControl, INotifyPropertyChanged
    {
        private object _current = new object();

        public ElementViewer()
        {
            InitializeComponent();
            _items.CollectionChanged += ItemsOnCollectionChanged;
        }

        private void ItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        _items[e.NewStartingIndex].PropertyChanged += OnPropertyChanged;
                            break;
                    }
                case NotifyCollectionChangedAction.Remove:
                    {
                        break;
                    }
            }
            Canvas.Invalidate();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            Canvas.Invalidate();
        }


        private int ZOOM = 100;
        private int GRID_SIZE = 16;
        private int GRID_POINT_SIZE = 2;
        private bool GRID_SHOW = true;
        private Color GRID_COLOR = Color.Silver;
        private Color ELM_COLOR = Color.Green;
        private Color ELM_COLOR_SELECT = Color.Blue;
        ObservableCollection<ElementUtz> _items = new ObservableCollection<ElementUtz>();
        private int _idUtz = 0;
        private int _idLayer = 0;
        private int _idFrames = 0;

        public int GridSize
        {
            get
            {
                return GRID_SIZE;
            }

            set
            {
                if (value < 5)
                    GRID_SIZE = 5;
                else
                    GRID_SIZE = value;
                Canvas.Invalidate();
                OnPropertyChanged();
            }
        }

        public int GridPointSize
        {
            get
            {
                return GRID_POINT_SIZE;
            }

            set
            {
                if (GRID_POINT_SIZE < 1)
                    GRID_POINT_SIZE = 1;
                else if (GRID_POINT_SIZE > GRID_SIZE / 4)
                    GRID_POINT_SIZE = GRID_SIZE / 4;
                else
                    GRID_POINT_SIZE = value;
                Canvas.Invalidate();
                OnPropertyChanged();
            }
        }

        public bool GridShow
        {
            get
            {
                return GRID_SHOW;
            }

            set
            {
                GRID_SHOW = value;
                Canvas.Invalidate();
                OnPropertyChanged();
            }
        }

        public int Zoom
        {
            get
            {
                return ZOOM;
            }

            set
            {
                if (value < 10)
                    ZOOM = 10;
                else if (value > 500)
                    ZOOM = 500;
                ZOOM = value;
                Canvas.Invalidate();
                OnPropertyChanged();
            }
        }

        public Color GridColor
        {
            get
            {
                return GRID_COLOR;
            }

            set
            {
                GRID_COLOR = value;
                OnPropertyChanged();
                Canvas.Invalidate();
            }
        }

        public ObservableCollection<ElementUtz> Items
        {
            get
            {
                return _items;
            }

            set
            {
                _items = value;
                foreach (ElementUtz item in _items)
                {
                    item.Parent = this;
                    item.PropertyChanged -= OnPropertyChanged;
                    item.PropertyChanged += OnPropertyChanged;
                }
                _items.CollectionChanged += ItemsOnCollectionChanged;
                Canvas.Invalidate();
            }
        }

        public Color ElmColor
        {
            get
            {
                return ELM_COLOR;
            }

            set
            {
                ELM_COLOR = value;
                OnPropertyChanged();
            }
        }

        public Color ElmColorSelect
        {
            get
            {
                return ELM_COLOR_SELECT;
            }

            set
            {
                ELM_COLOR_SELECT = value;
                OnPropertyChanged();
            }
        }

        public object Current
        {
            get
            {
                return _current;
            }

        }

        public int IdUtz
        {
            get
            {
                return _idUtz;
            }

            set
            {
                _idUtz = value;
            }
        }

        public int IdLayer
        {
            get
            {
                return _idLayer;
            }

            set
            {
                _idLayer = value;
            }
        }

        public int IdFrames
        {
            get
            {
                return _idFrames;
            }

            set
            {
                _idFrames = value;
            }
        }

        public ElementViewer(string FileName)
        {
            InitializeComponent();
        }

        private void Canvas_Click(object sender, EventArgs e)
        {
            MouseEventArgs t = ((MouseEventArgs)e);
            if (t == null) return;
            if (t.Button == MouseButtons.Left)
            {
                _current = _items.FirstOrDefault(x => x.OnClick(((MouseEventArgs)e).X, ((MouseEventArgs)e).Y));
            }
            this.OnClick(e);
            this.Focus();
        }

        private void Canvas_Paint(object sender, PaintEventArgs e)
        {
            #region GridShow
            if (GRID_SHOW)
            {
                for (int i = 0; i < this.Canvas.Width; i += GRID_SIZE)
                {
                    for (int j = 0; j < this.Canvas.Height; j += GRID_SIZE)
                        e.Graphics.FillRectangle(new SolidBrush(GRID_COLOR), i - GRID_POINT_SIZE/2, j - GRID_POINT_SIZE/2, GRID_POINT_SIZE, GRID_POINT_SIZE);
                }
            }
            #endregion
            #region ElementShow
            foreach (var item in _items)
            {
                item.OnPaint(e.Graphics);
            }
            
            #endregion
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            this.OnMouseMove(e);
        }
    }
}
