﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Imaging;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using DataProviderLib.DateBase;
using Editor;
using Gant;
using PetaPoco.DataAnnotations;
using Svg;

namespace ElmViewer
{
    [Serializable]
    public class ElementUtz : INotifyPropertyChanged
    {
        private static ElementViewer _parent;
        private SvgDocument svgDoc;
        private static int NUM = 1;
        private static float zoom = 1;
        private static int offset_x = 0;
        private static int offset_y = 0;


        public static void offset(int X, int Y)
        {
            offset_x = X;
            offset_y = Y;
        }
        public static void zoomset(float z)
        {
            zoom = z;
        }
        private int _fixId = '\0';
        public int Fix_ID
        {
            get
            {
                return _fixId;
            }

            set
            {
                _fixId = value;
            }
        }

        private char _type = '\0';
        [DisplayName(@"Тип элемента")]
        [Description("Описание элемента отсутствует!..")]
        [Category("Основное")]
        [ReadOnly(true)]
        public char Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                try
                {
                    svgDoc = SvgDocument.Open("C:\\SVG\\" + ((int)_type).ToString() + ".svg");
                }
                catch (Exception)
                {
                    Console.WriteLine("C:\\SVG\\" + ((int)_type).ToString() + ".svg");
                    //throw;
                }
                // 
                try
                {
                    var Provider = new DataProvider();
                    //
                    //foreach (var a in Provider.Db.Query<ElmParam>($"SELECT P.*, IFNULL(G.Title,'') AS `GroupName` FROM `Parameter` AS P LEFT JOIN `ParamGroup` AS G ON P.`Group` = G.id WHERE P.Id_ElementType = {(int)_type}"))
                    string str_SQL =
                        "SELECT P.id,P.Id_ElementType,P.Id_Parameter,P.Title,P.ValType,P.ReadOnly,P.Description,P.`Group`," +
                        "IFNULL(PF.Forse,P.`Force`) AS `Force`,P.Util,IFNULL(PF.Value,P.DefValue) AS `DefValue`,P.MinVal,P.MaxVal, " +
                        "IFNULL(G.Title,'') AS `GroupName` FROM `Parameter` AS P " +
                        "LEFT JOIN `ParamGroup` AS G ON P.`Group` = G.id " +
                        "LEFT JOIN `ParametrFrames` AS PF " +
                        $"ON PF.id_Param = P.Id_Parameter AND PF.id_Element = {this.Id} " + //AND PF.id_UTZ = {} AND PF.id_Frames = 10
                        $"WHERE P.Id_ElementType = {(int)_type}";
                    foreach (var a in Provider.Db.Query<ElmParam>(str_SQL))
                    {
                        a.Parent = this;
                        switch (a.Type)
                        {
                            case 0:
                                {
                                    var tmp = new ElmValue_d(a);
                                    a.Value = tmp;
                                    break;
                                }
                            case 1:
                                {
                                    var tmp = new ElmValue_i(a);
                                    a.Value = tmp;
                                    break;
                                }
                            case 2:
                                {
                                    var tmp = new ElmValue_b(a); ;
                                    a.Value = tmp;
                                    break;
                                }
                            case 3:
                                {
                                    var tmp = new ElmValue_a(a); //{ Value = a.DefVal }; ; // array
                                    a.Value = tmp;
                                    break;
                                }
                            case 4:
                                {
                                    var tmp = new ElmValue_l(a); //{ Value = a.DefVal }; ; // list
                                    a.Value = tmp;
                                    break;
                                }
                        }
                        a.ElementUtz = this;
                        _items.Add(a);
                    }

                }
                catch (Exception)
                {

                    //throw;
                }
                OnPropertyChanged();
            }
        }

        private int _id = 0;
        [DisplayName(@"Идентификатор")]
        [Description("Описание элемента отсутствует!..")]
        [Category("Основное")]
        [ReadOnly(true)]
        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        //
        private string _name = "";
        [DisplayName(@"Имя элемента")]
        [Description("Описание элемента отсутствует!..")]
        [Category("Основное")]
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }


        //
        private float _x1 = 0;
        public float X1
        {
            get
            {
                return (_x1 + offset_x) * zoom;
            }

            set
            {
                _x1 = value;
                OnPropertyChanged();
            }
        }

        //
        private float _y1 = 0;
        public float Y1
        {
            get
            {
                return (_y1 + offset_y) * zoom;
            }

            set
            {
                _y1 = value;
                OnPropertyChanged();
            }
        }

        //
        private float _x2 = 0;
        public float X2
        {
            get
            {
                return (_x2 + offset_x) * zoom;
            }

            set
            {
                _x2 = value;
                OnPropertyChanged();
            }
        }

        //
        private float _y2 = 0;
        public float Y2
        {
            get
            {
                return (_y2 + offset_y) * zoom;
            }

            set
            {
                _y2 = value;
                OnPropertyChanged();
            }
        }



        //
        private ObservableCollection<ElmParam> _items = new ObservableCollection<ElmParam>();
        [DisplayName(@"Имя элемента")]
        [Description("Описание элемента отсутствует!..")]
        [Category("Основное")]
        public ObservableCollection<ElmParam> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                _items.CollectionChanged -= ItemsOnCollectionChanged;
                _items.CollectionChanged += ItemsOnCollectionChanged;
            }
        }

        public ElementViewer Parent
        {
            get
            {
                return _parent;
            }

            set
            {
                _parent = value;
            }
        }



        public ElementUtz(string line)
        {
            _items.CollectionChanged += ItemsOnCollectionChanged;
            svgDoc = new SvgDocument();
            string[] s = line.Split('\t');
            if (s.Length >= 6)
            {
                this.Type = s[0][0];
                float x1 = this.X1;
                float x2 = this.X2;
                float y1 = this.Y1;
                float y2 = this.Y2;
                float.TryParse(s[1], out x1);
                float.TryParse(s[2], out y1);
                float.TryParse(s[3], out x2);
                float.TryParse(s[4], out y2);
                this.X1 = x1;
                this.X2 = x2;
                this.Y1 = y1;
                this.Y2 = y2;
                this.Name = s[5];
            }
        }


        // public Attribute[] attrs = new Attribute[10];
        public ElementUtz()
        {
            svgDoc = new SvgDocument();
            //Name = "Элемент № " + NUM++;
            _items.CollectionChanged += ItemsOnCollectionChanged;
        }

        private void ItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            OnPropertyChanged();
        }

        public void OnPaint(Graphics g)
        {
            //var dx = Math.Abs(X1 - X2);
            //var dy = Math.Abs(Y1 - Y2);
            //var dLengh = Math.Sqrt(dx * dx + dy * dy);
            //SizeF s_size = g.MeasureString(Name, new Font("Arial", 8, FontStyle.Regular));
            //g.DrawString(Name, new Font("Arial", 8, FontStyle.Regular), new SolidBrush(Color.Black), dx - s_size.Width / 2, dy);
            //g.DrawLine(new Pen(Color.RoyalBlue, 4), X1, Y1, X2, Y2);
            float _x = (Math.Abs(X1 - X2) / 2 + Math.Min(X1, X2));
            float _y = (Math.Abs(Y1 - Y2) / 2 + Math.Min(Y1, Y2));
            string s = Name;
            SizeF s_size = g.MeasureString(s, new Font("Arial", 10, FontStyle.Bold));
            //======================================================
            float A = (float)(Math.Atan2(Y1 - Y2, X1 - X2) / Math.PI * 180);
            if (A >= 90) A -= 180;
            if (A <= -90) A += 180;
            g.DrawLine(new Pen(Color.RoyalBlue, 4), new Point((int)X1, (int)Y1), new Point((int)X2, (int)Y2));
            if (A == 0)
                _x -= (svgDoc.Width / 2);
            else
                _x += (svgDoc.Width / 2);
            _y -= (svgDoc.Height / 2);
            g.TranslateTransform(_x, _y);
            g.RotateTransform(A);
            //============================
            try
            {
                SvgElement o = svgDoc.GetElementById("Main");
                if (o != null)
                {
                    o.Fill = new SvgColourServer(Color.LimeGreen); //new SvgColourServer(Color.RoyalBlue);
                    o.Stroke = new SvgColourServer(Color.RoyalBlue);
                    o.StrokeWidth = 3;
                }
                svgDoc.Draw(g);

            }
            catch (Exception)
            {
            }
            //============================
            g.RotateTransform(-A);
            g.TranslateTransform(-_x, -_y);
            _x = (Math.Abs(X1 - X2) / 2 + Math.Min(X1, X2));
            _y = (Math.Abs(Y1 - Y2) / 2 + Math.Min(Y1, Y2));
            g.TranslateTransform(_x, _y);
            g.RotateTransform(A);
            g.DrawString(s, new Font("Arial", 8, FontStyle.Bold), new SolidBrush(Color.Black), 0 - s_size.Width / 2, svgDoc.Bounds.Height / 2);
            g.RotateTransform(-A);
            g.TranslateTransform(-_x, -_y);
            //=======================================================

        }

        public bool OnClick(int X, int Y)
        {
            int _x = (int)(Math.Abs(X1 - X2) / 2 + Math.Min(X1, X2));
            int _y = (int)(Math.Abs(Y1 - Y2) / 2 + Math.Min(Y1, Y2));

            if (
                (_x - (svgDoc.Width / 2) < X) &&
                (_y - (svgDoc.Height / 2) < Y) &&
                (_x + (svgDoc.Width / 2) > X) &&
                (_y + (svgDoc.Height / 2) > Y)
                )
                return true;


            var dx = (X1 - X2);
            var dy = (Y1 - Y2);
            var dLengh = Math.Sqrt(dx * dx + dy * dy);
            dx = (X1 - X);
            dy = (Y1 - Y);
            var dLengh1 = Math.Sqrt(dx * dx + dy * dy);
            dx = (X2 - X);
            dy = (Y2 - Y);
            var dLengh2 = Math.Sqrt(dx * dx + dy * dy);
            //isSelect = Math.Abs(dLengh - (dLengh1 + dLengh2)) < 0.2;
            //if ((_x1 < X) && (_y1 < Y) && (_x2 > X) && (_y2 > Y))
            //{
            //    return true;
            //}
            return Math.Abs(dLengh - (dLengh1 + dLengh2)) < 0.2;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public void SetFrame(int Layer, int Frame)
        {

            OnPropertyChanged("SetFrame");
        }

    }

    public class ElmParam : INotifyPropertyChanged
    {

        private static ElementUtz _parent;
        private int _id;
        private ElementUtz _elementUtz;
        private int _idParameter;
        private string _name;
        private ElmValue _value;
        private int _type;
        private bool _readOnly;
        private string _category;
        private string _description;
        private bool _force;
        private double _maxVal;
        private double _minVal;
        private double _defVal;
        private string _util;


        [Column("Title")]
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public ElmValue Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                _value.PropertyChanged += ValueOnPropertyChanged;
                OnPropertyChanged();
            }
        }

        [Column("ReadOnly")]
        public bool ReadOnly
        {
            get
            {
                return _readOnly;
            }

            set
            {
                _readOnly = value;
                OnPropertyChanged();
            }
        }
        [Column("GroupName")]
        public string Category
        {
            get
            {
                return _category;
            }

            set
            {
                _category = value;
                OnPropertyChanged();
            }
        }
        [Column("Description")]
        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }
        [Column("ValType")]
        public int Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
            }
        }
        [Column("Force")]
        public bool Force
        {
            get
            {
                return _force;
            }

            set
            {
                _force = value;
            }
        }
        [Column("Util")]
        public string Util
        {
            get
            {
                return _util;
            }

            set
            {
                _util = value;
            }
        }
        [Column("DefValue")]
        public double DefVal
        {
            get
            {
                return _defVal;
            }

            set
            {
                _defVal = value;
            }
        }
        [Column("id")]
        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }
        [Column("Id_Parameter")]
        public int Id_Parameter
        {
            get
            {
                return _idParameter;
            }

            set
            {
                _idParameter = value;
            }
        }

        public ElementUtz ElementUtz
        {
            get
            {
                return _elementUtz;
            }

            set
            {
                _elementUtz = value;
            }
        }

        public double MaxVal
        {
            get
            {
                return _maxVal;
            }

            set
            {
                _maxVal = value;
            }
        }

        public double MinVal
        {
            get
            {
                return _minVal;
            }

            set
            {
                _minVal = value;
            }
        }

        public ElementUtz Parent
        {
            get
            {
                return _parent;
            }

            set
            {
                _parent = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private void ValueOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            OnPropertyChanged(propertyChangedEventArgs.PropertyName);
        }
    }

    public class ElmValue : INotifyPropertyChanged
    {
        protected ElmParam _elmParam;
        private static DataProvider Provider = new DataProvider();
        private bool _force;
        [DisplayName("Имитация")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool Force
        {
            get
            {
                return _force;
            }
            set
            {
                _force = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void ParamWrite(double val)
        {
            Provider.Db.Execute("INSERT INTO `training`.`ParametrFrames` (`id_UTZ`, `id_Layers`, `id_Frames`, `id_Element`, `id_Param`, `Value`, `Forse`) VALUES " +
                                $"({_elmParam.Parent.Parent.IdUtz}, {_elmParam.Parent.Parent.IdLayer}, {_elmParam.Parent.Parent.IdFrames}, {_elmParam.ElementUtz.Id}, {_elmParam.Id_Parameter}, {val}, {_force}) " +
                                $"ON DUPLICATE KEY UPDATE `Value` = {val}, `Forse` = {_force}");
        }
    }

    public class ElmValue_b : ElmValue
    {
        
        private bool _value;

        public ElmValue_b(ElmParam a)
        {
            this._elmParam = a;
            _value = (bool)(Math.Abs(_elmParam.DefVal) > 1e-6);
        }

        [DisplayName("Значение")]
        [TypeConverter(typeof(BooleanTypeConverter))]
        public bool Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                ParamWrite(_value ? 1:0);
            }
        }

        public override string ToString()
        {
            return (_value ? "Да" : "Нет") + (base.Force ? " (F)" : "");
        }
    }
    public class ElmValue_d : ElmValue
    {

        private double _value;

        public ElmValue_d(ElmParam a)
        {
            this._elmParam = a;
            _value = _elmParam.DefVal;
        }

        [DisplayName("Значение")]
        public double Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                if (_elmParam.MaxVal > _elmParam.MinVal)
                {
                    if (value > _elmParam.MaxVal)
                        _value = _elmParam.MaxVal;
                    if (value < _elmParam.MinVal)
                        _value = _elmParam.MinVal;
                }
                ParamWrite(_value);
            }
        }

        public override string ToString()
        {
            return Value.ToString() + (base.Force ? " (F)" : "");
        }
    }
    public class ElmValue_i : ElmValue
    {

        private int _value;

        public ElmValue_i(ElmParam a)
        {
            this._elmParam = a;
            _value = (int)Math.Round(_elmParam.DefVal);
        }

        [DisplayName("Значение")]
        public int Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                if (_elmParam.MaxVal > _elmParam.MinVal)
                {
                    if (value > Math.Round(_elmParam.MaxVal))
                        _value = (int)Math.Round(_elmParam.MaxVal);
                    if (value < Math.Round(_elmParam.MinVal))
                        _value = (int)Math.Round(_elmParam.MinVal);
                }
                ParamWrite(_value);
            }
        }
        public override string ToString()
        {
            return Value.ToString() + (base.Force ? " (F)" : "");
        }
    }
    public class ElmValue_a : ElmValue
    {


        //private BindingList<> _bindingList = ;
        private Dictionary<int, double> _value = new Dictionary<int, double>(10);

        public ElmValue_a(ElmParam a)
        {
            this._elmParam = a;
        }

        [DisplayName("Значение")]
        [Editor(typeof(ConvertGH), typeof(UITypeEditor))]
        public Dictionary<int, double> Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                OnPropertyChanged();
            }
        }

        public override string ToString()
        {
            return "Коллекция" + (base.Force ? " (F)" : "");
        }
    }
 
    public class ElmValue_l : ElmValue
    {

        private int _value;
        listVal _listVal = new listVal();
        private List<listVal> _list = new List<listVal>();

        public ElmValue_l(ElmParam a)
        {
            this._elmParam = a;
            _value = (int)Math.Round(_elmParam.DefVal);
        }

        [DisplayName("Значение")]
        [TypeConverter(typeof(PostTypeConverter))]
        public int Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                ParamWrite(_value);
            }
        }

        public List<listVal> List
        {
            get
            {
                return _list;
            }

            set
            {
                _list = value;
            }
        }

        public listVal ListVal
        {
            get
            {
                return _listVal;
            }

            set
            {
                _listVal = value;
            }
        }

      

        public override string ToString()
        {
            return Value.ToString() + (base.Force ? " (F)" : "");
        }

        public class listVal
        {
            private int _key;
            private string _value;

            public int Key
            {
                get
                {
                    return _key;
                }

                set
                {
                    _key = value;
                }
            }
            public string Value
            {
                get
                {
                    return _value;
                }

                set
                {
                    _value = value;
                }
            }
            public override string ToString()
            {
                return _value.ToString();
            }

            
        }

        
    }




    public class BooleanTypeConverter : BooleanConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context,
          CultureInfo culture,
          object value,
          Type destType)
        {
            return (bool)value ? "Да" : "Нет";
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
          CultureInfo culture,
          object value)
        {
            return (string)value == "Да";
        }

    }


    public class PostTypeConverter : DoubleConverter
    {
        public static Dictionary<string, PostTypeConverter> convDict = new Dictionary<string, PostTypeConverter>(); 
        List<ListVal> _listVals;

        public PostTypeConverter(int id, string name)
        {
            _listVals = new List<ListVal>();
            _listVals.Clear();
            var Provider = new DataProvider();
            foreach (var a in Provider.Db.Query<ListVal>($"SELECT * FROM ListVal WHERE id_list = {id}"))
            {
                _listVals.Add(a);
            }
            convDict[name] = this;
        }
        public PostTypeConverter()
        {
            //var Provider = new DataProvider();
            //foreach (var a in Provider.Db.Query<ListVal>($"SELECT * FROM ListVal "))
            //{
            //    _listVals.Add(a);
            //}
        }


        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
        {
            
            ListVal tmp = null;
            string str = "";
            if (value is int)
            {
                str = convDict[context.PropertyDescriptor.Name]._listVals.FirstOrDefault(x => x.Key == (int)value).Value;
                return str;
            }
            if (value is ListVal)
            {
                tmp = (ListVal)value;
                str = tmp.Value;
                return str;
            }
            return value;
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is ListVal)
            {
                return ((ListVal)value).Key;
            }
            if (value is string)
            {
                ListVal tmp = convDict[context.PropertyDescriptor.Name]._listVals.FirstOrDefault(x => x.Value == (string)value);
                int index = 0;
                if (tmp != null)
                    index = tmp.Key;
                return index;
            }
            return value;
        }


        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            return true;
        }

        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // возвращаем список строк из настроек программы
            // (базы данных, интернет и т.д.)
            return new StandardValuesCollection(convDict[context.PropertyDescriptor.Name]._listVals);
        }


    }

    public class ConvertGH : UITypeEditor
    {

        /// <summary>
        /// Реализация метода редактирования
        /// </summary>
        public override Object EditValue(
          ITypeDescriptorContext context,
          IServiceProvider provider,
          Object value)
        {
            if ((context != null) && (provider != null))
            {
                IWindowsFormsEditorService svc =
                  (IWindowsFormsEditorService)
                  provider.GetService(typeof(IWindowsFormsEditorService));

                if (svc != null)
                {
                    using (GH_Editor formGhEditor =
                      new GH_Editor((Dictionary<int, double>)value))
                    {
                        if (svc.ShowDialog(formGhEditor) == DialogResult.OK)
                        {
                            value = formGhEditor.Value;
                        }
                    }
                }
            }

            return base.EditValue(context, provider, value);
        }

        /// <summary>
        /// Возвращаем стиль редактора - модальное окно
        /// </summary>
        public override UITypeEditorEditStyle GetEditStyle(
          ITypeDescriptorContext context)
        {
            if (context != null)
                return UITypeEditorEditStyle.Modal;
            else
                return base.GetEditStyle(context);
        }

    }

    class ListVal
    {
        private int _id_list;
        private int _key;
        private string _value;
        [Column("id_list")]
        public int Id_list
        {
            get
            {
                return _id_list;
            }

            set
            {
                _id_list = value;
            }
        }
        [Column("Key")]
        public int Key
        {
            get
            {
                return _key;
            }

            set
            {
                _key = value;
            }
        }
        [Column("Val")]
        public string Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
            }
        }

        public override string ToString()
        {
            return _value;
        }
    }
}