﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElmViewer
{
    public partial class GH_Editor : Form
    {
        private BindingList<KeyVal> _keyVals = new BindingList<KeyVal>();
        private Dictionary<int, double> _value;
        public Dictionary<int, double> Value
        {
            get
            {
                _value.Clear();
                foreach (var item in _keyVals)
                {
                    _value.Add(item.Key, item.Value);
                }
                return _value;
            }
            set
            {
                _value = value;
            }
        }
        public GH_Editor(Dictionary<int, double> value)
        {
            InitializeComponent();
            _value = value;
            _keyVals.ListChanged += KeyValsOnListChanged;
            _keyVals.Clear();
            _value.ToList().ForEach(x => _keyVals.Add(new KeyVal() { Key = x.Key, Value = x.Value }));

            dataGridView1.DataSource = _keyVals;
            //====================
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.Navy;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font(dataGridView1.Font, FontStyle.Bold);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.Location = new Point(8, 8);
            dataGridView1.Size = new Size(500, 250);
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            dataGridView1.GridColor = Color.Black;
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
            dataGridView1.Dock = DockStyle.Fill;
            //===================
        }

        private void KeyValsOnListChanged(object sender, ListChangedEventArgs e)
        {
            foreach (var item in _keyVals)
            {
                item.PropertyChanged -= ItemOnPropertyChanged;
                item.PropertyChanged += ItemOnPropertyChanged;
            }

        }

        private void ItemOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            var tmp = _keyVals.OrderBy(x => x.Key).ToList();
            _keyVals.Clear();
            tmp.ForEach(x => _keyVals.Add(new KeyVal() { Key = x.Key, Value = x.Value }));
            pictureBox1.Invalidate();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            _keyVals.Add(new KeyVal());
            //this.dataGridView1.Rows.Add();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.SelectedRows.Count > 0 && this.dataGridView1.SelectedRows[0].Index != this.dataGridView1.Rows.Count - 1)
            {
                this.dataGridView1.Rows.RemoveAt(this.dataGridView1.SelectedRows[0].Index);
            }
            pictureBox1.Invalidate();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            #region grid

            try
            {

                var s = "Value";
                var s1 = "Key";
                int _height = pictureBox1.Height - 32;
                int _width = pictureBox1.Width - 32;
                SizeF s_size = e.Graphics.MeasureString(s, this.Font);
                SizeF s_size1 = e.Graphics.MeasureString(s1, this.Font);
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 16, pictureBox1.Height - 32, 16, 16);
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 16, pictureBox1.Height - 32, pictureBox1.Width - 16,
                    pictureBox1.Height - 32);
                e.Graphics.DrawString(s, this.Font, new SolidBrush(Color.Black), pictureBox1.Width - 18 - s_size.Width,
                    pictureBox1.Height - 32);
                e.Graphics.DrawString(s1, this.Font, new SolidBrush(Color.Black), 18, 16);
                double maxVal = Double.MinValue;
                double minVal = Double.MaxValue;
                int maxKey = Int32.MinValue;
                int minKey = Int32.MaxValue;
                foreach (var item in _keyVals)
                {
                    maxVal = Math.Max(maxVal, item.Value);
                    minVal = Math.Min(minVal, item.Value);
                    maxKey = Math.Max(maxKey, item.Key);
                    minKey = Math.Min(minKey, item.Key);
                }
                var kw = _width/Math.Abs(maxVal - minVal);
                var kh = _height/Math.Abs(maxKey - minKey);
                double old_v = 0;
                int old_k = 0;
                var first = true;
                foreach (var item in _keyVals)
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.Red), (float) ((item.Value - minVal)*kw) + 16 - 2,
                        _height - (float) ((item.Key - minKey)*kh) - 2, 4, 4);
                    if (!first)
                    {
                        e.Graphics.DrawLine(new Pen(Color.CornflowerBlue, 2), (float) ((old_v - minVal)*kw) + 16,
                            _height - (float) ((old_k - minKey)*kh), (float) ((item.Value - minVal)*kw) + 16,
                            _height - (float) ((item.Key - minKey)*kh));
                    }
                    first = false;
                    old_v = item.Value;
                    old_k = item.Key;
                }

                #endregion
            }
            catch
            {
            }
        }
    }

    public class KeyVal : INotifyPropertyChanged
    {
        private int _key;
        private double _value;

        public int Key
        {
            get
            {
                return _key;
            }

            set
            {
                _key = value;
                OnPropertyChanged();
            }
        }

        public double Value
        {
            get
            {
                return _value;
            }

            set
            {
                _value = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
