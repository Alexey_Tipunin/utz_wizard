﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using DataProviderLib.DateBase;
using PetaPoco.DataAnnotations;


namespace TestSystem
{
    [Serializable]
    [TableName("Question")]
    [PrimaryKey("id_Layer,Num")]
    public class Question : INotifyPropertyChanged
    {
        //private static DataProvider Provider = new DataProvider();
        private string _name = string.Empty;
        private int _type = 1;
        private bool _usetime = true;
        private int _id_layer = 0;
        private int _id_frame = 0;
        private List<Replys> _tmpReplys = new List<Replys>();

        private ObservableCollection<Replys> _listReply = new ObservableCollection<Replys>();

        public Question()
        {
            _listReply.CollectionChanged += ListReplyOnCollectionChanged;
            //Provider.Db.Insert(this);
        }

        private void ListReplyOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            DataProvider Provider = new DataProvider();
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        try
                        {
                            _listReply[e.NewStartingIndex].Id_frame = this.id_frame;
                            _listReply[e.NewStartingIndex].Id_layers = this.id_layer;
                            _listReply[e.NewStartingIndex].Order = e.NewStartingIndex;
                            _listReply[e.NewStartingIndex].PropertyChanged -= OnPropertyChanged;
                            _listReply[e.NewStartingIndex].PropertyChanged += OnPropertyChanged;
                            Provider.Db.Insert("Replys", "", _listReply[e.NewStartingIndex]);
                        }
                        catch (Exception)
                        {//
                            //throw;
                        }
                        break;
                    }
                case NotifyCollectionChangedAction.Remove:
                    {
                        try
                        {
                            Provider.Db.Execute("DELETE FROM `training`.`Replys` WHERE  `id_Layer`=@0 AND `Num`=@1", _id_layer, _id_frame);
                            var i = 0;
                            foreach (var reply in _listReply)
                            {
                                reply.Order = i++;
                                Provider.Db.Insert("Replys", "", reply);
                            }
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                        break;
                    }
            }
            _tmpReplys = _listReply.ToList();
            OnPropertyChanged();
            //throw new NotImplementedException();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
        DataProvider Provider = new DataProvider();
        Replys replys = (sender as Replys);
            if (replys != null)
                Provider.Db.Execute($"UPDATE `training`.`Replys` SET `Title`='{replys.Name}', " +
                                    $"`Reply`='{replys.Reply}' " +
                                    $"WHERE  `id_Layer`='{replys.Id_layers}' " +
                                    $"AND `Num`='{replys.Id_frame}' " +
                                    $"AND `Order`='{replys.Order}'");
        }

        [Column("Title")]
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }
        [Column("Type")]
        public int Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
                OnPropertyChanged();
            }
        }


        [Ignore]
        public ObservableCollection<Replys> Items
        {
            get
            {
                return _listReply;
            }

            set
            {
                _listReply = value;
                _listReply.CollectionChanged += ListReplyOnCollectionChanged;
            }
        }
        [Column("UseTime")]
        public bool UseTime
        {
            get
            {
                return _usetime;
            }

            set
            {
                _usetime = value;
                OnPropertyChanged();
            }
        }

        [Column("id_Layer")]
        public int id_layer
        {
            get
            {
                return _id_layer;
            }

            set
            {
                _id_layer = value;
            }
        }
        [Column("Num")]
        public int id_frame
        {
            get
            {
                return _id_frame;
            }

            set
            {
                _id_frame = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged( string propertyName = null)
        {
        DataProvider Provider = new DataProvider();
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            if (this._id_frame > 0 && this._id_layer > 0)
                Provider.Db.Execute("UPDATE `training`.`Question` SET `Type`=@2, `Title`=@3, `UseTime`=@4 WHERE  `id_Layer`=@0 AND `Num`=@1", _id_layer, _id_frame, _type,_name, _usetime);
        }
    }
}