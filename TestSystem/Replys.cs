﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DataProviderLib.DateBase;
using PetaPoco.DataAnnotations;

namespace TestSystem
{
    [Serializable]
    [TableName("Question")]
    [PrimaryKey("id")]
    public class Replys: INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        private static DataProvider Provider = new DataProvider();

        private string _name = String.Empty;
        private int _reply = 0;
        private int _id_layers = 0;
        private int _id_frame = 0;
        private int _order = 0;

        [Column("Title")]
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        [Column("Reply")]
        public int Reply
        {
            get
            {
                return _reply;
            }

            set
            {
                _reply = value;
                OnPropertyChanged();
            }
        }


        [Column("id_Layer")]
        public int Id_layers
        {
            get
            {
                return _id_layers;
            }

            set
            {
                _id_layers = value;
            }
        }

        [Column("Num")]
        public int Id_frame
        {
            get
            {
                return _id_frame;
            }

            set
            {
                _id_frame = value;
            }
        }
        [Column("Order")]
        public int Order
        {
            get
            {
                return _order;
            }

            set
            {
                _order = value;
                OnPropertyChanged();
            }
        }

        protected virtual void OnPropertyChanged( string propertyName = null)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        }
    }


}