﻿namespace TestSystem
{
    partial class TestSysUTZ
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.question_group = new System.Windows.Forms.GroupBox();
            this.question = new System.Windows.Forms.TextBox();
            this.select_panel = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.time_use = new System.Windows.Forms.CheckBox();
            this.AddReply = new System.Windows.Forms.Button();
            this.panel_tool = new System.Windows.Forms.Panel();
            this.type_q3 = new System.Windows.Forms.RadioButton();
            this.type_q2 = new System.Windows.Forms.RadioButton();
            this.type_q1 = new System.Windows.Forms.RadioButton();
            this.reply_group = new System.Windows.Forms.GroupBox();
            this.reply_panel = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.question_group.SuspendLayout();
            this.select_panel.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel_tool.SuspendLayout();
            this.reply_group.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // question_group
            // 
            this.question_group.Controls.Add(this.question);
            this.question_group.Dock = System.Windows.Forms.DockStyle.Top;
            this.question_group.Location = new System.Drawing.Point(0, 0);
            this.question_group.MinimumSize = new System.Drawing.Size(0, 40);
            this.question_group.Name = "question_group";
            this.question_group.Padding = new System.Windows.Forms.Padding(5);
            this.question_group.Size = new System.Drawing.Size(1038, 63);
            this.question_group.TabIndex = 8;
            this.question_group.TabStop = false;
            this.question_group.Text = "Вопрос";
            // 
            // question
            // 
            this.question.Dock = System.Windows.Forms.DockStyle.Fill;
            this.question.Location = new System.Drawing.Point(5, 18);
            this.question.MinimumSize = new System.Drawing.Size(4, 20);
            this.question.Multiline = true;
            this.question.Name = "question";
            this.question.Size = new System.Drawing.Size(1028, 40);
            this.question.TabIndex = 0;
            // 
            // select_panel
            // 
            this.select_panel.Controls.Add(this.panel3);
            this.select_panel.Controls.Add(this.panel_tool);
            this.select_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.select_panel.Location = new System.Drawing.Point(5, 5);
            this.select_panel.Name = "select_panel";
            this.select_panel.Size = new System.Drawing.Size(1038, 31);
            this.select_panel.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.time_use);
            this.panel3.Controls.Add(this.AddReply);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(838, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 31);
            this.panel3.TabIndex = 5;
            // 
            // time_use
            // 
            this.time_use.AutoSize = true;
            this.time_use.Dock = System.Windows.Forms.DockStyle.Left;
            this.time_use.Location = new System.Drawing.Point(0, 0);
            this.time_use.Name = "time_use";
            this.time_use.Size = new System.Drawing.Size(157, 31);
            this.time_use.TabIndex = 3;
            this.time_use.Text = "Ограничить время ответа";
            this.time_use.UseVisualStyleBackColor = true;
            // 
            // AddReply
            // 
            this.AddReply.Dock = System.Windows.Forms.DockStyle.Right;
            this.AddReply.Image = global::TestSystem.Properties.Resources.add_16;
            this.AddReply.Location = new System.Drawing.Point(162, 0);
            this.AddReply.Name = "AddReply";
            this.AddReply.Size = new System.Drawing.Size(38, 31);
            this.AddReply.TabIndex = 0;
            this.AddReply.Text = "Добавить вопрос";
            this.AddReply.UseVisualStyleBackColor = true;
            this.AddReply.Click += new System.EventHandler(this.AddReply_Click);
            // 
            // panel_tool
            // 
            this.panel_tool.Controls.Add(this.type_q3);
            this.panel_tool.Controls.Add(this.type_q2);
            this.panel_tool.Controls.Add(this.type_q1);
            this.panel_tool.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_tool.Location = new System.Drawing.Point(0, 0);
            this.panel_tool.Name = "panel_tool";
            this.panel_tool.Size = new System.Drawing.Size(1038, 31);
            this.panel_tool.TabIndex = 4;
            // 
            // type_q3
            // 
            this.type_q3.AutoSize = true;
            this.type_q3.Checked = true;
            this.type_q3.Dock = System.Windows.Forms.DockStyle.Left;
            this.type_q3.Location = new System.Drawing.Point(504, 0);
            this.type_q3.Name = "type_q3";
            this.type_q3.Size = new System.Drawing.Size(218, 31);
            this.type_q3.TabIndex = 2;
            this.type_q3.TabStop = true;
            this.type_q3.Tag = "3";
            this.type_q3.Text = "Вопрос с указанием верного порядка";
            this.type_q3.UseVisualStyleBackColor = true;
            this.type_q3.CheckedChanged += new System.EventHandler(this.type_CheckedChanged);
            // 
            // type_q2
            // 
            this.type_q2.AutoSize = true;
            this.type_q2.Dock = System.Windows.Forms.DockStyle.Left;
            this.type_q2.Location = new System.Drawing.Point(239, 0);
            this.type_q2.Name = "type_q2";
            this.type_q2.Size = new System.Drawing.Size(265, 31);
            this.type_q2.TabIndex = 1;
            this.type_q2.Tag = "2";
            this.type_q2.Text = "Вопрос с выбором нескольких верных ответов";
            this.type_q2.UseVisualStyleBackColor = true;
            this.type_q2.CheckedChanged += new System.EventHandler(this.type_CheckedChanged);
            // 
            // type_q1
            // 
            this.type_q1.AutoSize = true;
            this.type_q1.Dock = System.Windows.Forms.DockStyle.Left;
            this.type_q1.Location = new System.Drawing.Point(0, 0);
            this.type_q1.Name = "type_q1";
            this.type_q1.Size = new System.Drawing.Size(239, 31);
            this.type_q1.TabIndex = 0;
            this.type_q1.Tag = "1";
            this.type_q1.Text = "Вопрос с выбором одного верного ответа";
            this.type_q1.UseVisualStyleBackColor = true;
            this.type_q1.CheckedChanged += new System.EventHandler(this.type_CheckedChanged);
            // 
            // reply_group
            // 
            this.reply_group.Controls.Add(this.reply_panel);
            this.reply_group.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reply_group.Location = new System.Drawing.Point(0, 63);
            this.reply_group.MinimumSize = new System.Drawing.Size(0, 20);
            this.reply_group.Name = "reply_group";
            this.reply_group.Padding = new System.Windows.Forms.Padding(5);
            this.reply_group.Size = new System.Drawing.Size(1038, 504);
            this.reply_group.TabIndex = 10;
            this.reply_group.TabStop = false;
            this.reply_group.Text = "Ответ";
            // 
            // reply_panel
            // 
            this.reply_panel.AutoScroll = true;
            this.reply_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reply_panel.Location = new System.Drawing.Point(5, 18);
            this.reply_panel.Name = "reply_panel";
            this.reply_panel.Padding = new System.Windows.Forms.Padding(5);
            this.reply_panel.Size = new System.Drawing.Size(1028, 481);
            this.reply_panel.TabIndex = 0;
            this.reply_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.reply_panel_Paint);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 63);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1038, 3);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitter1);
            this.panel1.Controls.Add(this.reply_group);
            this.panel1.Controls.Add(this.question_group);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(5, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1038, 567);
            this.panel1.TabIndex = 0;
            // 
            // TestSysUTZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.select_panel);
            this.Name = "TestSysUTZ";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Size = new System.Drawing.Size(1048, 608);
            this.question_group.ResumeLayout(false);
            this.question_group.PerformLayout();
            this.select_panel.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel_tool.ResumeLayout(false);
            this.panel_tool.PerformLayout();
            this.reply_group.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox question_group;
        private System.Windows.Forms.TextBox question;
        private System.Windows.Forms.Panel select_panel;
        private System.Windows.Forms.CheckBox time_use;
        private System.Windows.Forms.RadioButton type_q3;
        private System.Windows.Forms.RadioButton type_q2;
        private System.Windows.Forms.RadioButton type_q1;
        private System.Windows.Forms.GroupBox reply_group;
        private System.Windows.Forms.Panel reply_panel;
        private System.Windows.Forms.Button AddReply;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel_tool;
        private System.Windows.Forms.Panel panel3;
    }
}
