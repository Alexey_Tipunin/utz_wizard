﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataProviderLib.DateBase;

namespace TestSystem
{

    public partial class TestSysUTZ : UserControl
    {
        
        Question _question = new Question();
        public TestSysUTZ()
        {
            InitializeComponent();
            _question.PropertyChanged += QuestionOnPropertyChanged;
            _question.Items.CollectionChanged += ItemsOnCollectionChanged;
            //Update();
        }

        public Question Question
        {
            get
            {
                return _question;
            }

            set
            {
                _question = value;
                reply_panel.Controls.Clear();
#region 1
                foreach (var item in _question.Items)
                {
                    UpdateControl(item);
                }
#endregion
                _question.PropertyChanged -= QuestionOnPropertyChanged;
                _question.PropertyChanged += QuestionOnPropertyChanged;
                _question.Items.CollectionChanged -= ItemsOnCollectionChanged;
                _question.Items.CollectionChanged += ItemsOnCollectionChanged;
                this.question.DataBindings.Clear();
                this.question.DataBindings.Add("Text", _question, "Name");
                this.time_use.DataBindings.Clear();
                this.time_use.DataBindings.Add("Checked", _question, "UseTime");
                Update();
            }
        }

        private void ItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        UpdateControl(_question.Items[e.NewStartingIndex]);
                        break;
                    }
                case NotifyCollectionChangedAction.Remove:
                    {
                        if (_question.Items.Count == 0)
                            panel_tool.Enabled = true;
                        break;
                    }
            }
            Update();
        }

        private void UpdateControl(Replys itemReplys)
        {
            Control IsVal = new Control();
            if (_question.Type == 1)
            {
                IsVal = new RadioButton();
                ((RadioButton)IsVal).Click += Reply_CheckedChanged;
                IsVal.Dock = DockStyle.Left;
                ((RadioButton)IsVal).CheckAlign = ContentAlignment.MiddleCenter;
                IsVal.Size = new Size(40, 40);
            }
            if (_question.Type == 2)
            {
                IsVal = new CheckBox();
                IsVal.Dock = DockStyle.Left;
                ((CheckBox)IsVal).CheckAlign = ContentAlignment.MiddleCenter;
                IsVal.Size = new Size(40, 40);
            }
            if (_question.Type == 3)
            {
                IsVal = new Button();
                ((Button)IsVal).Click += Reply_Checking;
                ((Button)IsVal).Tag = itemReplys;
                IsVal.Dock = DockStyle.Left;
                IsVal.Size = new Size(40, 40);
            }
            Button remove_b = new Button();
            remove_b.Text = @"&Удалить";
            remove_b.Dock = DockStyle.Right;
            remove_b.Click += new EventHandler(Remove_reply_Click);

            TextBox Edit = new TextBox();
            Edit.Multiline = true;
            Edit.Dock = DockStyle.Fill;

            UserControl panel = new UserControl();
            panel.Height = Edit.Height * 2 - 6;
            panel.Dock = DockStyle.Top;


            panel.Controls.Add(Edit);
            panel.Controls.Add(IsVal);
            panel.Controls.Add(remove_b);

            if (_question.Type == 1 || _question.Type == 2)
            {
                IsVal.DataBindings.Add("Checked", itemReplys, "Reply", true);
            }
            else if (_question.Type == 3)
            {
                IsVal.DataBindings.Add("Text", itemReplys, "Reply", true);
                //IsVal.DataBindings.Add("Tag", bindingList[e.NewIndex], "Reply", true);
            }
            Edit.DataBindings.Add("Text", itemReplys, "Name");
            remove_b.Tag = itemReplys;
            reply_panel.Controls.Add(panel);
            panel.BringToFront();
            panel_tool.Enabled = false;
        }


        private void Remove_reply_Click(object sender, EventArgs e)
        {
            var userControl = sender as Button;
            if (userControl != null)
            {
                var testS = (userControl.Tag as Replys);
                _question.Items.Remove(testS);
                reply_panel.Controls.Remove(userControl.Parent);
            }
        }

        private void QuestionOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            Update();
        }

        public new void Update()
        {
            switch (_question.Type)
            {
                case 1:
                    {
                        type_q1.Checked = true;
                        break;
                    }
                case 2:
                    {
                        type_q2.Checked = true;
                        break;
                    }
                case 3:
                    {
                        type_q3.Checked = true;
                        break;
                    }
            }

            panel_tool.Enabled = _question.Items.Count == 0;
        }

        private void AddReply_Click(object sender, EventArgs e)
        {
            _question.Items.Add(new Replys());
        }

        private void type_CheckedChanged(object sender, EventArgs e)
        {
            var o = (sender as RadioButton);
            if (o == null) return;
            if (o.Checked)
            {
                var type = _question.Type;
                int.TryParse(o.Tag.ToString(), out type);
                _question.Type = type;
            }
        }


        private void Reply_CheckedChanged(object sender, EventArgs e)
        {
            foreach (var item in _question.Items)
            {
                item.Reply = 0;
            }
            ((RadioButton)sender).Checked = true;
        }

        private void Reply_Checking(object sender, EventArgs e)
        {
            int num = 0;
            Replys tag = null;
            int[] next = new int[_question.Items.Count + 1];
            var control = sender as Control;
            if (control != null)
            {
                tag = (control.Tag as Replys);
                if (tag != null)
                {
                    num = (int)tag.Reply;
                    num++;
                    if (num > _question.Items.Count) num = 0;
                }
            }
            foreach (var item in _question.Items)
            {
                if (item.Reply > _question.Items.Count) item.Reply = 0;
                next[item.Reply] = 1;
            }
            next[0] = 0;
            for (int j = num; j < next.Length; j++)
            {
                if (next[j] == 0)
                {
                    num = j;
                    break;
                }
                else
                {
                    num = 0;
                }
            }
            //control.Text = num.ToString();
            tag.Reply = num;
            //control.EndEdit;
            //control.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            //itemTest.

        }

        private void reply_panel_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
