﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DataProviderLib.DateBase;
using PetaPoco.DataAnnotations;

namespace TestSystem
{
    [Serializable]
    [TableName("UserEvents")]
    [PrimaryKey("id_Layer,Num")]
    public class UserEvents : INotifyPropertyChanged
    {
        private string _name = string.Empty;
        private int _type = 1;
        private bool _usetime = true;
        private int _id_layer = 0;
        private int _id_frame = 0;
        private ObservableCollection<eventParam> _eventParam;

        public UserEvents()
        {
            _eventParam = new ObservableCollection<eventParam>();
            _eventParam.CollectionChanged += EventParamOnCollectionChanged;
        }

        private void EventParamOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            DataProvider Provider = new DataProvider();
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        try
                        {
                            _eventParam[e.NewStartingIndex].Id_frame = this.id_frame;
                            _eventParam[e.NewStartingIndex].Id_layers = this.id_layer;
                            _eventParam[e.NewStartingIndex].Order = e.NewStartingIndex;
                            _eventParam[e.NewStartingIndex].PropertyChanged -= OnPropertyChanged;
                            _eventParam[e.NewStartingIndex].PropertyChanged += OnPropertyChanged;
                            Provider.Db.Insert("eventParam", "", _eventParam[e.NewStartingIndex]);
                        }
                        catch (Exception)
                        {
                            //
                            //throw;
                        }
                        break;
                    }
                case NotifyCollectionChangedAction.Remove:
                    {
                        try
                        {
                            Provider.Db.Execute("DELETE FROM `training`.`eventParam` WHERE  `id_Layer`=@0 AND `Num`=@1",
                                _id_layer, _id_frame);
                            var i = 0;
                            foreach (var param in _eventParam)
                            {

                                param.Order = i++;
                                Provider.Db.Insert("eventParam", "", param);
                            }
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                        break;
                    }
            }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            DataProvider Provider = new DataProvider();
            eventParam eParam = (sender as eventParam);
            if (eParam != null)
                Provider.Db.Execute($"UPDATE `training`.`eventParam` SET `Id_elm`='{eParam.Id_elm}', " +
                                $"`Id_param`='{eParam.Id_param}' ," +
                                $"`Value`='{eParam.Value}' ," +
                                $"`Noze`='{eParam.Noze}' ," +
                                $"`Cmp`='{eParam.Cmp}' " +
                                $"WHERE  `id_Layer`='{eParam.Id_layers}' " +
                                $"AND `Num`='{eParam.Id_frame}' " +
                                $"AND `Order`='{eParam.Order}'");
        }

        [Ignore]
        public ObservableCollection<eventParam> Items
        {
            get
            {
                return _eventParam;
            }

            set
            {
                _eventParam = value;
                _eventParam.CollectionChanged -= EventParamOnCollectionChanged;
                _eventParam.CollectionChanged += EventParamOnCollectionChanged;
            }
        }

        [Column("Title")]
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }
        [Column("Type")]
        public int Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
                OnPropertyChanged();
            }
        }

        [Column("UseTime")]
        public bool UseTime
        {
            get
            {
                return _usetime;
            }

            set
            {
                _usetime = value;
                OnPropertyChanged();
            }
        }

        [Column("id_Layer")]
        public int id_layer
        {
            get
            {
                return _id_layer;
            }

            set
            {
                _id_layer = value;
            }
        }
        [Column("Num")]
        public int id_frame
        {
            get
            {
                return _id_frame;
            }

            set
            {
                _id_frame = value;
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            DataProvider Provider = new DataProvider();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            if (this._id_frame > 0 && this._id_layer > 0)
                Provider.Db.Execute("UPDATE `training`.`UserEvents` SET `Type`=@2, `Title`=@3, `UseTime`=@4 WHERE  `id_Layer`=@0 AND `Num`=@1", _id_layer, _id_frame, _type, _name, _usetime);
        }
    }

    public class eventParam : INotifyPropertyChanged
    {
        private int _id_layers = 0;
        private int _id_frame = 0;
        private int _order = 0;
        private int id_elm;
        private int id_param;
        private double value;
        private double noze;
        private int _cmp; // 1 - равно, 2 - меньше, 3 - больше;

        [Column("id_Layer")]
        public int Id_layers
        {
            get
            {
                return _id_layers;
            }

            set
            {
                _id_layers = value;
            }
        }

        [Column("Num")]
        public int Id_frame
        {
            get
            {
                return _id_frame;
            }

            set
            {
                _id_frame = value;
            }
        }
        [Column("Order")]
        public int Order
        {
            get
            {
                return _order;
            }

            set
            {
                _order = value;
                OnPropertyChanged();
            }
        }
        public int Id_elm
        {
            get
            {
                return id_elm;
            }

            set
            {
                id_elm = value;
            }
        }

        public int Id_param
        {
            get
            {
                return id_param;
            }

            set
            {
                id_param = value;
            }
        }

        public double Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
                OnPropertyChanged();
            }
        }

        public double Noze
        {
            get
            {
                return noze;
            }

            set
            {
                noze = value;
                OnPropertyChanged();
            }
        }

        public int Cmp
        {
            get
            {
                return _cmp;
            }

            set
            {
                _cmp = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        }
    }
}