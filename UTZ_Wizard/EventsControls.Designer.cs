﻿namespace UTZ_Wizard
{
    partial class EventsControls
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.discription = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.radioOR = new System.Windows.Forms.RadioButton();
            this.radioAND = new System.Windows.Forms.RadioButton();
            this.time_use = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.radioOR);
            this.panel2.Controls.Add(this.radioAND);
            this.panel2.Controls.Add(this.time_use);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(919, 26);
            this.panel2.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.discription);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(339, 0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(3);
            this.panel4.Size = new System.Drawing.Size(356, 26);
            this.panel4.TabIndex = 6;
            // 
            // discription
            // 
            this.discription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.discription.Location = new System.Drawing.Point(3, 3);
            this.discription.Name = "discription";
            this.discription.Size = new System.Drawing.Size(350, 20);
            this.discription.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(695, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(19, 26);
            this.panel3.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(256, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "Описание:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radioOR
            // 
            this.radioOR.AutoSize = true;
            this.radioOR.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioOR.Location = new System.Drawing.Point(120, 0);
            this.radioOR.Name = "radioOR";
            this.radioOR.Size = new System.Drawing.Size(136, 26);
            this.radioOR.TabIndex = 2;
            this.radioOR.TabStop = true;
            this.radioOR.Tag = "2";
            this.radioOR.Text = "Параметры по \"ИЛИ\"";
            this.radioOR.UseVisualStyleBackColor = true;
            this.radioOR.CheckedChanged += new System.EventHandler(this.radioOR_CheckedChanged);
            // 
            // radioAND
            // 
            this.radioAND.AutoSize = true;
            this.radioAND.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioAND.Location = new System.Drawing.Point(0, 0);
            this.radioAND.Name = "radioAND";
            this.radioAND.Size = new System.Drawing.Size(120, 26);
            this.radioAND.TabIndex = 1;
            this.radioAND.TabStop = true;
            this.radioAND.Tag = "1";
            this.radioAND.Text = "Параметры по \"И\"";
            this.radioAND.UseVisualStyleBackColor = true;
            this.radioAND.CheckedChanged += new System.EventHandler(this.radioOR_CheckedChanged);
            // 
            // time_use
            // 
            this.time_use.AutoSize = true;
            this.time_use.Dock = System.Windows.Forms.DockStyle.Right;
            this.time_use.Location = new System.Drawing.Point(714, 0);
            this.time_use.Name = "time_use";
            this.time_use.Size = new System.Drawing.Size(120, 26);
            this.time_use.TabIndex = 0;
            this.time_use.Text = "Ограничить время";
            this.time_use.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Location = new System.Drawing.Point(834, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 26);
            this.button1.TabIndex = 3;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // mainPanel
            // 
            this.mainPanel.AutoScroll = true;
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 26);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(919, 283);
            this.mainPanel.TabIndex = 2;
            // 
            // EventsControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.panel2);
            this.Name = "EventsControls";
            this.Size = new System.Drawing.Size(919, 309);
            this.Load += new System.EventHandler(this.EventsControls_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioOR;
        private System.Windows.Forms.RadioButton radioAND;
        private System.Windows.Forms.CheckBox time_use;
        private System.Windows.Forms.TextBox discription;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel mainPanel;
    }
}
