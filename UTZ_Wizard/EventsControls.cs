﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataProviderLib.DateBase;
using ElmViewer;
using PetaPoco.DataAnnotations;
using TestSystem;
using UTZ_Wizard.Annotations;

namespace UTZ_Wizard
{
    public partial class EventsControls : UserControl
    {
        BindingList<elmUTZ> elemList = new BindingList<elmUTZ>();
        BindingList<proertyElm> elemProertyList = new BindingList<proertyElm>();
        ObservableCollection<eventParam>  items = new ObservableCollection<eventParam>();

        public ObservableCollection<eventParam> Items
        {
            get
            {
                return items;
            }
            set
            {
                items = value;
                items.CollectionChanged -= ItemsOnCollectionChanged;
                items.CollectionChanged += ItemsOnCollectionChanged;
            }
        }

        private UserEvents _userEvents;

        public UserEvents userEvents
        {
            get
            {
                return _userEvents;
            }

            set
            {
                _userEvents = value;
                //reply_panel.Controls.Clear();
                #region 1
               
                foreach (var item in _userEvents.Items)
                {
                    UpdateControl(item);
                }
                
                #endregion
                _userEvents.PropertyChanged -= QuestionOnPropertyChanged;
                _userEvents.PropertyChanged += QuestionOnPropertyChanged;
                _userEvents.Items.CollectionChanged -= ItemsOnCollectionChanged;
                _userEvents.Items.CollectionChanged += ItemsOnCollectionChanged;
                this.discription.DataBindings.Clear();
                this.discription.DataBindings.Add("Text", _userEvents, "Name");
                this.time_use.DataBindings.Clear();
                this.time_use.DataBindings.Add("Checked", _userEvents, "UseTime");
                Update();
            }
        }

        private void QuestionOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            Update();
        }

        public new void Update()
        {
            switch (_userEvents.Type)
            {
                case 1:
                    {
                        radioAND.Checked = true;
                        break;
                    }
                case 2:
                    {
                        radioOR.Checked = true;
                        break;
                    }
            }

            //panel_tool.Enabled = _question.Items.Count == 0;
        }

        public EventsControls()
        {
            InitializeComponent();

            //dataGridView1.DataSource = Items;
        }

        private void ItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        UpdateControl(_userEvents.Items[e.NewStartingIndex]);
                        break;
                    }
                case NotifyCollectionChangedAction.Remove:
                    {
                        //if (_userEvents.Items.Count == 0)
                            //panel_tool.Enabled = true;
                        break;
                    }
            }
            Update();
        }


        private void UpdateControl(eventParam item)
        {
            controlEventParam control = new controlEventParam();
            control.delete.Tag = item;
            control.EventParam = item;
            control.Dock = DockStyle.Top;
            control.delete.Click -= Remove_reply_Click;
            control.delete.Click += Remove_reply_Click;
            mainPanel.Controls.Add(control);
        }

        private void Remove_reply_Click(object sender, EventArgs e)
        {
            var userControl = sender as Button;
            if (userControl != null)
            {
                var testS = (userControl.Tag as eventParam);
                _userEvents.Items.Remove(testS);
                mainPanel.Controls.Remove(userControl.Parent.Parent.Parent);

            }
        }


        private void EventsControls_Load(object sender, EventArgs e)
        {
            elemList.Clear();
            DataProvider Provider = new DataProvider();
            foreach (var a in Provider.Db.Query<elmUTZ>("SELECT * FROM training.Element where Element.`Type` <> 112 order by Element.`Type`, Element.Name"))
            {
                elemList.Add(a);
            }
        }
        


        private proertyElm tmp;



        private void radioOR_CheckedChanged(object sender, EventArgs e)
        {
            var o = (sender as RadioButton);
            if (o == null) return;
            if (o.Checked)
            {
                var type = _userEvents.Type;
                int.TryParse(o.Tag.ToString(), out type);
                _userEvents.Type = type;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            _userEvents.Items.Add(new eventParam());
        }
    }

    public class elmUTZ
    {
        private int elm_id = 0;
        private int elm_type = 0;
        private string name = "";

        [Column("id")]
        public int Elm_id
        {
            get
            {
                return elm_id;
            }

            set
            {
                elm_id = value;
            }
        }
        [Column("Type")]
        public int Elm_type
        {
            get
            {
                return elm_type;
            }

            set
            {
                elm_type = value;
            }
        }
        [Column("Name")]
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
    }

    public class proertyElm
    {
        private int id;
        private int id_param;
        private int type;
        private string name;
        private double val = 0;
        private double noze = 0;

        [Column("id")]
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        [Column("ValType")]
        public int Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        [Column("Title")]
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        [Ignore]
        public double Val
        {
            get
            {
                return val;
            }

            set
            {
                val = value;
            }
        }
        [Ignore]
        public double Noze
        {
            get
            {
                return noze;
            }

            set
            {
                noze = value;
            }
        }

        [Column("Id_Parameter")]
        public int Id_param
        {
            get
            {
                return id_param;
            }

            set
            {
                id_param = value;
            }
        }
    }

   
}
