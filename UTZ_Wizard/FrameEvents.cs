﻿namespace UTZ_Wizard
{
    class FrameEvents
    {
        private int elementUTZ;
        private int id_param;

        public int ElementUTZ
        {
            get
            {
                return elementUTZ;
            }

            set
            {
                elementUTZ = value;
            }
        }

        public int Id_param
        {
            get
            {
                return id_param;
            }

            set
            {
                id_param = value;
            }
        }
    }
}