﻿namespace UTZ_Wizard
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusZOOM = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolZOOM = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.NowUTZ = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.User = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.panel_PropertyGrid = new System.Windows.Forms.Panel();
            this.elm_property_grid = new PropertyGridEx.PropertyGridEx();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.сбросToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.обновитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelElem = new System.Windows.Forms.Panel();
            this.elementViewer1 = new ElmViewer.ElementViewer();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.PanelFrameTool = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panelGant = new System.Windows.Forms.Panel();
            this.utzGant1 = new Gant.UtzGant();
            this.propertyGridEx1 = new PropertyGridEx.PropertyGridEx();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.statusZOOM.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel_PropertyGrid.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelElem.SuspendLayout();
            this.panelGant.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusZOOM
            // 
            this.statusZOOM.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.statusZOOM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1,
            this.toolZOOM,
            this.toolStripStatusLabel2});
            this.statusZOOM.Location = new System.Drawing.Point(0, 710);
            this.statusZOOM.Name = "statusZOOM";
            this.statusZOOM.Size = new System.Drawing.Size(1189, 22);
            this.statusZOOM.TabIndex = 0;
            this.statusZOOM.Text = "statusZOOM";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // toolZOOM
            // 
            this.toolZOOM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.toolStripSeparator1,
            this.toolStripMenuItem4,
            this.toolStripMenuItem3,
            this.toolStripMenuItem2,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7});
            this.toolZOOM.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolZOOM.Name = "toolZOOM";
            this.toolZOOM.Size = new System.Drawing.Size(51, 20);
            this.toolZOOM.Text = "100 %";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(157, 6);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(160, 22);
            this.toolStripMenuItem4.Text = "200%";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(160, 22);
            this.toolStripMenuItem3.Text = "150%";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(160, 22);
            this.toolStripMenuItem2.Text = "100%";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(160, 22);
            this.toolStripMenuItem5.Text = "75%";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(160, 22);
            this.toolStripMenuItem6.Text = "50%";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(160, 22);
            this.toolStripMenuItem7.Text = "25%";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NowUTZ,
            this.toolStripButton2,
            this.toolStripButton1,
            this.User,
            this.toolStripSeparator2,
            this.toolStripButton5,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripButton6,
            this.toolStripSeparator3,
            this.toolStripButton7});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1189, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // NowUTZ
            // 
            this.NowUTZ.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NowUTZ.Image = global::UTZ_Wizard.Properties.Resources.Document_16;
            this.NowUTZ.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NowUTZ.Name = "NowUTZ";
            this.NowUTZ.Size = new System.Drawing.Size(23, 22);
            this.NowUTZ.Text = "toolStripButton1";
            this.NowUTZ.Click += new System.EventHandler(this.NowUTZ_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::UTZ_Wizard.Properties.Resources.folderopen1;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::UTZ_Wizard.Properties.Resources.clockwise_arrow16;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // User
            // 
            this.User.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.User.Image = global::UTZ_Wizard.Properties.Resources.users16_1;
            this.User.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(23, 22);
            this.User.Text = "toolStripButton3";
            this.User.Click += new System.EventHandler(this.User_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = global::UTZ_Wizard.Properties.Resources.skip_backward1_16;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "toolStripButton5";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::UTZ_Wizard.Properties.Resources.play_16;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton3";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = global::UTZ_Wizard.Properties.Resources.pause_16;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "toolStripButton4";
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = global::UTZ_Wizard.Properties.Resources.skip_forward1_16;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "toolStripButton6";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // panel_PropertyGrid
            // 
            this.panel_PropertyGrid.Controls.Add(this.elm_property_grid);
            this.panel_PropertyGrid.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel_PropertyGrid.Location = new System.Drawing.Point(989, 138);
            this.panel_PropertyGrid.Name = "panel_PropertyGrid";
            this.panel_PropertyGrid.Size = new System.Drawing.Size(200, 547);
            this.panel_PropertyGrid.TabIndex = 3;
            this.panel_PropertyGrid.Visible = false;
            // 
            // elm_property_grid
            // 
            this.elm_property_grid.CategoryForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.elm_property_grid.ContextMenuStrip = this.contextMenuStrip1;
            // 
            // 
            // 
            this.elm_property_grid.DocCommentDescription.AutoEllipsis = true;
            this.elm_property_grid.DocCommentDescription.Cursor = System.Windows.Forms.Cursors.Default;
            this.elm_property_grid.DocCommentDescription.Location = new System.Drawing.Point(3, 18);
            this.elm_property_grid.DocCommentDescription.Name = "";
            this.elm_property_grid.DocCommentDescription.Size = new System.Drawing.Size(194, 37);
            this.elm_property_grid.DocCommentDescription.TabIndex = 1;
            this.elm_property_grid.DocCommentImage = null;
            // 
            // 
            // 
            this.elm_property_grid.DocCommentTitle.Cursor = System.Windows.Forms.Cursors.Default;
            this.elm_property_grid.DocCommentTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.elm_property_grid.DocCommentTitle.Location = new System.Drawing.Point(3, 3);
            this.elm_property_grid.DocCommentTitle.Name = "";
            this.elm_property_grid.DocCommentTitle.Size = new System.Drawing.Size(194, 15);
            this.elm_property_grid.DocCommentTitle.TabIndex = 0;
            this.elm_property_grid.DocCommentTitle.UseMnemonic = false;
            this.elm_property_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elm_property_grid.Location = new System.Drawing.Point(0, 0);
            this.elm_property_grid.Name = "elm_property_grid";
            this.elm_property_grid.Size = new System.Drawing.Size(200, 547);
            this.elm_property_grid.TabIndex = 0;
            // 
            // 
            // 
            this.elm_property_grid.ToolStrip.Location = new System.Drawing.Point(0, 0);
            this.elm_property_grid.ToolStrip.Name = "";
            this.elm_property_grid.ToolStrip.Size = new System.Drawing.Size(200, 25);
            this.elm_property_grid.ToolStrip.TabIndex = 1;
            this.elm_property_grid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.elm_property_grid_PropertyValueChanged);
            this.elm_property_grid.SelectedGridItemChanged += new System.Windows.Forms.SelectedGridItemChangedEventHandler(this.elm_property_grid_SelectedGridItemChanged);
            this.elm_property_grid.SelectedObjectsChanged += new System.EventHandler(this.elm_property_grid_SelectedObjectsChanged);
            this.elm_property_grid.Paint += new System.Windows.Forms.PaintEventHandler(this.elm_property_grid_Paint);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сбросToolStripMenuItem,
            this.toolStripSeparator4,
            this.обновитьToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(129, 54);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // сбросToolStripMenuItem
            // 
            this.сбросToolStripMenuItem.Name = "сбросToolStripMenuItem";
            this.сбросToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.сбросToolStripMenuItem.Text = "Сброс";
            this.сбросToolStripMenuItem.Click += new System.EventHandler(this.сбросToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(125, 6);
            // 
            // обновитьToolStripMenuItem
            // 
            this.обновитьToolStripMenuItem.Name = "обновитьToolStripMenuItem";
            this.обновитьToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.обновитьToolStripMenuItem.Text = "Обновить";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.panelElem);
            this.panel3.Controls.Add(this.splitter1);
            this.panel3.Controls.Add(this.panel_PropertyGrid);
            this.panel3.Controls.Add(this.splitter3);
            this.panel3.Controls.Add(this.panelGant);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 25);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1189, 685);
            this.panel3.TabIndex = 6;
            // 
            // panelElem
            // 
            this.panelElem.BackColor = System.Drawing.SystemColors.Info;
            this.panelElem.Controls.Add(this.elementViewer1);
            this.panelElem.Controls.Add(this.splitter2);
            this.panelElem.Controls.Add(this.PanelFrameTool);
            this.panelElem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelElem.Location = new System.Drawing.Point(0, 138);
            this.panelElem.Name = "panelElem";
            this.panelElem.Size = new System.Drawing.Size(986, 547);
            this.panelElem.TabIndex = 5;
            // 
            // elementViewer1
            // 
            this.elementViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementViewer1.ElmColor = System.Drawing.Color.Green;
            this.elementViewer1.ElmColorSelect = System.Drawing.Color.Blue;
            this.elementViewer1.GridColor = System.Drawing.Color.Silver;
            this.elementViewer1.GridPointSize = 2;
            this.elementViewer1.GridShow = true;
            this.elementViewer1.GridSize = 16;
            this.elementViewer1.IdFrames = 0;
            this.elementViewer1.IdLayer = 0;
            this.elementViewer1.IdUtz = 0;
            this.elementViewer1.Items = ((System.Collections.ObjectModel.ObservableCollection<ElmViewer.ElementUtz>)(resources.GetObject("elementViewer1.Items")));
            this.elementViewer1.Location = new System.Drawing.Point(0, 0);
            this.elementViewer1.Name = "elementViewer1";
            this.elementViewer1.Size = new System.Drawing.Size(986, 294);
            this.elementViewer1.TabIndex = 2;
            this.elementViewer1.Zoom = 100;
            this.elementViewer1.Click += new System.EventHandler(this.elementViewer1_Click);
            this.elementViewer1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.elementViewer1_MouseMove);
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(0, 294);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(986, 3);
            this.splitter2.TabIndex = 3;
            this.splitter2.TabStop = false;
            // 
            // PanelFrameTool
            // 
            this.PanelFrameTool.BackColor = System.Drawing.SystemColors.Control;
            this.PanelFrameTool.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelFrameTool.Location = new System.Drawing.Point(0, 297);
            this.PanelFrameTool.Name = "PanelFrameTool";
            this.PanelFrameTool.Size = new System.Drawing.Size(986, 250);
            this.PanelFrameTool.TabIndex = 6;
            this.PanelFrameTool.Visible = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(986, 138);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 547);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 135);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1189, 3);
            this.splitter3.TabIndex = 1;
            this.splitter3.TabStop = false;
            // 
            // panelGant
            // 
            this.panelGant.Controls.Add(this.utzGant1);
            this.panelGant.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelGant.Location = new System.Drawing.Point(0, 0);
            this.panelGant.Name = "panelGant";
            this.panelGant.Size = new System.Drawing.Size(1189, 135);
            this.panelGant.TabIndex = 0;
            // 
            // utzGant1
            // 
            this.utzGant1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.utzGant1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.utzGant1.Enabled = false;
            this.utzGant1.GantElms = null;
            this.utzGant1.Id_Utz = 0;
            this.utzGant1.Location = new System.Drawing.Point(0, 0);
            this.utzGant1.Name = "utzGant1";
            this.utzGant1.SelectFrame = 0;
            this.utzGant1.SelectLine = 0;
            this.utzGant1.Size = new System.Drawing.Size(1189, 135);
            this.utzGant1.TabIndex = 0;
            this.utzGant1.AddFrames += new System.EventHandler<Gant.UTZ_Frame>(this.utzGant1_ChangedFrames);
            this.utzGant1.ChangedFrames += new System.EventHandler<Gant.UTZ_Frame>(this.utzGant1_ChangedFrames);
            this.utzGant1.Load += new System.EventHandler(this.utzGant1_Load);
            // 
            // propertyGridEx1
            // 
            this.propertyGridEx1.CategoryForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            // 
            // 
            // 
            this.propertyGridEx1.DocCommentDescription.AutoEllipsis = true;
            this.propertyGridEx1.DocCommentDescription.Cursor = System.Windows.Forms.Cursors.Default;
            this.propertyGridEx1.DocCommentDescription.Location = new System.Drawing.Point(3, 18);
            this.propertyGridEx1.DocCommentDescription.Name = "";
            this.propertyGridEx1.DocCommentDescription.Size = new System.Drawing.Size(0, 52);
            this.propertyGridEx1.DocCommentDescription.TabIndex = 1;
            this.propertyGridEx1.DocCommentImage = null;
            // 
            // 
            // 
            this.propertyGridEx1.DocCommentTitle.Cursor = System.Windows.Forms.Cursors.Default;
            this.propertyGridEx1.DocCommentTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.propertyGridEx1.DocCommentTitle.Location = new System.Drawing.Point(3, 3);
            this.propertyGridEx1.DocCommentTitle.Name = "";
            this.propertyGridEx1.DocCommentTitle.Size = new System.Drawing.Size(0, 0);
            this.propertyGridEx1.DocCommentTitle.TabIndex = 0;
            this.propertyGridEx1.DocCommentTitle.UseMnemonic = false;
            this.propertyGridEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridEx1.Location = new System.Drawing.Point(0, 0);
            this.propertyGridEx1.Name = "propertyGridEx1";
            this.propertyGridEx1.Size = new System.Drawing.Size(200, 547);
            this.propertyGridEx1.TabIndex = 0;
            // 
            // 
            // 
            this.propertyGridEx1.ToolStrip.AccessibleName = "Панель инструментов";
            this.propertyGridEx1.ToolStrip.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.propertyGridEx1.ToolStrip.AllowMerge = false;
            this.propertyGridEx1.ToolStrip.AutoSize = false;
            this.propertyGridEx1.ToolStrip.CanOverflow = false;
            this.propertyGridEx1.ToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.propertyGridEx1.ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.propertyGridEx1.ToolStrip.Location = new System.Drawing.Point(0, 1);
            this.propertyGridEx1.ToolStrip.Name = "";
            this.propertyGridEx1.ToolStrip.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
            this.propertyGridEx1.ToolStrip.Size = new System.Drawing.Size(200, 25);
            this.propertyGridEx1.ToolStrip.TabIndex = 1;
            this.propertyGridEx1.ToolStrip.TabStop = true;
            this.propertyGridEx1.ToolStrip.Text = "PropertyGridToolBar";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton7.Text = "toolStripButton7";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 732);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusZOOM);
            this.Name = "MainForm";
            this.Text = "Main Form UTZ Wizard";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusZOOM.ResumeLayout(false);
            this.statusZOOM.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel_PropertyGrid.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panelElem.ResumeLayout(false);
            this.panelGant.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusZOOM;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel_PropertyGrid;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel panelGant;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripButton NowUTZ;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripDropDownButton toolZOOM;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private ElmViewer.ElementViewer elementViewer1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Panel PanelFrameTool;
        private System.Windows.Forms.Panel panelElem;
        private System.Windows.Forms.Splitter splitter2;
        private Gant.UtzGant utzGant1;
        private System.Windows.Forms.ToolStripButton User;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private PropertyGridEx.PropertyGridEx elm_property_grid;
        private PropertyGridEx.PropertyGridEx propertyGridEx1;
        private System.Windows.Forms.ToolStripMenuItem сбросToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обновитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
    }
}

