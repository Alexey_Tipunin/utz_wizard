﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using DataProviderLib.DateBase;
using Editor;
using MySql.Data.MySqlClient;
using ElmViewer;
using Gant;
using PropertyGridEx;
using TestSystem;
using PropertyGridEx = PropertyGridEx.PropertyGridEx;

namespace UTZ_Wizard
{
    public partial class MainForm : Form
    {
        static UTZ _utz = new UTZ();
        public MainForm()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void utzGant1_Click(object sender, EventArgs e)
        {
            try
            {
                this.elm_property_grid.SelectedObject = utzGant1.GantElms[utzGant1.SelectLine][utzGant1.SelectFrame];
            }
            catch (Exception)
            {
                utzGant1.SelectFrame = 1;
            }

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            elementViewer1.Items.Clear();
            Form openUtzForm = new OpenUTZ();
            openUtzForm.StartPosition = FormStartPosition.CenterParent;
            openUtzForm.ShowDialog();
            if (_utz.LayerUtz != null)
            {
                utzGant1.GantElms = _utz.LayerUtz;
                utzGant1.Id_Utz = _utz.ID;
            }
            if (_utz.ElementUtz != null)
            {
                
                elementViewer1.Items = _utz.ElementUtz;
                elementViewer1.IdUtz = _utz.ID;
 
            }
            this.utzGant1.Enabled = true;
        }

        private void NowUTZ_Click(object sender, EventArgs e)
        {
            Form createUtzForm = new CreateUTZ();
            createUtzForm.StartPosition = FormStartPosition.CenterParent;
            createUtzForm.ShowDialog();
            if (_utz.LayerUtz != null) utzGant1.GantElms = _utz.LayerUtz;
            if (_utz.ElementUtz != null) elementViewer1.Items = _utz.ElementUtz;
            this.utzGant1.Enabled = true;
        }

        private void elementViewer1_Click(object sender, EventArgs e)
        {
            if (elementViewer1.Current != null && utzGant1.GantElms[utzGant1.SelectLine][utzGant1.SelectFrame] != null)
                this.elm_property_grid.SelectedObject = elementViewer1.Current;
            else
                this.elm_property_grid.SelectedObject = elementViewer1.Items.FirstOrDefault(x => x.Type == 65);
        }


        private void toolStripButton1_Click(object sender, EventArgs e)
        {

            var o = new ElementUtz();
            elementViewer1.Items.Clear();
            FileDialog fileDialog = new OpenFileDialog();
            fileDialog.ShowDialog();
            StreamReader sr = new StreamReader(fileDialog.FileName);
            o = null;
            var line = sr.ReadLine();
            while (line != null)
            {
                o = new ElementUtz(line);
                //switch (line[0])
                //{
                //    case 'f': { o = new ElementUtz(line); break; }
                //    case 'F': { o = new ElementUtz(line); break; }
                //    case 'p': { o = new ElementUtz(line); break; }
                //    case 'r': { o = new ElementUtz(line); break; }
                //    case 't': { o = new ElementUtz(line); break; }
                //    case 'z': { o = new ElementUtz(line); break; }
                //}
                if (o != null)
                {/*
                    string[] s = line.Split('\t');
                    o.x1 = int.Parse(s[1]);
                    o.y1 = int.Parse(s[2]);// + 16*4;
                    o.x2 = int.Parse(s[3]);
                    o.y2 = int.Parse(s[4]);// + 16*4;
                    o.NAME = s[5];
                  */
                    elementViewer1.Items.Add(o);
                    o = null;
                }
                line = sr.ReadLine();
            }
            sr.Close();

        }

        private void elm_property_grid_SelectedObjectsChanged(object sender, EventArgs e)
        {
            var propertyGrid = sender as global::PropertyGridEx.PropertyGridEx;
            if (propertyGrid != null)
            {
                if (propertyGrid.SelectedObject == null)
                {
                    propertyGrid.Parent.Visible = false;
                }
                else
                {
                    ElementUtz elmUtz = (propertyGrid.SelectedObject as ElementUtz);
                    if (elmUtz != null)
                    {
                        elmUtz.Items.Clear();
/***/
                        try
                        {
                            var Provider = new DataProvider();
                            //
                            //foreach (var a in Provider.Db.Query<ElmParam>($"SELECT P.*, IFNULL(G.Title,'') AS `GroupName` FROM `Parameter` AS P LEFT JOIN `ParamGroup` AS G ON P.`Group` = G.id WHERE P.Id_ElementType = {(int)_type}"))
                            string str_SQL =
                                "SELECT P.id,P.Id_ElementType,P.Id_Parameter,P.Title,P.ValType,P.ReadOnly,P.Description,P.`Group`," +
                                "IFNULL(PF.Forse,P.`Force`) AS `Force`,P.Util,IFNULL(PF.Value,P.DefValue) AS `DefValue`,P.MinVal,P.MaxVal, " +
                                "IFNULL(G.Title,'') AS `GroupName` FROM `Parameter` AS P " +
                                "LEFT JOIN `ParamGroup` AS G ON P.`Group` = G.id " +
                                "LEFT JOIN `ParametrFrames` AS PF " +
                                $"ON PF.id_Param = P.Id_Parameter AND PF.id_Element = {elmUtz.Id} AND PF.id_UTZ = {_utz.ID} AND PF.id_Frames = {utzGant1.SelectFrame} " +
                                $"WHERE P.Id_ElementType = {(int)elmUtz.Type}";
                            foreach (var a in Provider.Db.Query<ElmParam>(str_SQL))
                            {
                                a.Parent = elmUtz;
                                switch (a.Type)
                                {
                                    case 0:
                                        {
                                            var tmp = new ElmValue_d(a);
                                            a.Value = tmp;
                                            break;
                                        }
                                    case 1:
                                        {
                                            var tmp = new ElmValue_i(a);
                                            a.Value = tmp;
                                            break;
                                        }
                                    case 2:
                                        {
                                            var tmp = new ElmValue_b(a); ;
                                            a.Value = tmp;
                                            break;
                                        }
                                    case 3:
                                        {
                                            var tmp = new ElmValue_a(a); //{ Value = a.DefVal }; ; // array
                                            a.Value = tmp;
                                            break;
                                        }
                                    case 4:
                                        {
                                            var tmp = new ElmValue_l(a); //{ Value = a.DefVal }; ; // list
                                            a.Value = tmp;
                                            break;
                                        }
                                }
                                a.ElementUtz = elmUtz;
                                elmUtz.Items.Add(a);
                            }

                        }
                        catch (Exception)
                        {

                            //throw;
                        }
/****/
                        propertyGrid.Item.Clear();
                        propertyGrid.ShowCustomProperties = true;
                        propertyGrid.Item.Add("Имя элемента", elmUtz.Name, false, "", "", true);
                        propertyGrid.Item[propertyGrid.Item.Count - 1].IsReadOnly = true;
                        propertyGrid.Item.Add("ID элемента", elmUtz.Id, false, "", "", true);
                        propertyGrid.Item[propertyGrid.Item.Count - 1].IsReadOnly = true;
                        propertyGrid.Item.Add("Тип элемента", elmUtz.Type, false, "", "", true);
                        propertyGrid.Item[propertyGrid.Item.Count - 1].IsReadOnly = true;
                        propertyGrid.Item.Add("Тип элемента", (int)elmUtz.Type, false, "", "", true);
                        propertyGrid.Item[propertyGrid.Item.Count - 1].IsReadOnly = true;
                        propertyGrid.Item.Add("Fix ID", elmUtz.Id, false, "", "", true);
                        propertyGrid.Item[propertyGrid.Item.Count - 1].IsReadOnly = true;

                        if (elmUtz.Id > 0)
                        {


                            foreach (var item in elmUtz.Items)
                            {
                                string s = item.Util.ToString().Trim();
                                if (item.Util.ToString().Trim().Length > 0)
                                    s = " (" + s + ")";
                                object tmp;
                                if (!item.Force)
                                {
                                    tmp = item.Value;
                                    //propertyGrid.Item.Add(item.Name, item.Value, item.ReadOnly, item.Category, item.Description, true);
                                    propertyGrid.Item.Add(item.Name + s, ref tmp, "Value", item.ReadOnly, item.Category,
                                        item.Description, true);
                                    if (item.Type == 3) // 0 - double; 1- int; 2- bool; 3- array
                                        propertyGrid.Item[propertyGrid.Item.Count - 1].CustomEditor = new ConvertGH();
                                    if (item.Type == 2) // 0 - double; 1- int; 2- bool; 3- array
                                        propertyGrid.Item[propertyGrid.Item.Count - 1].CustomTypeConverter =
                                            new BooleanTypeConverter();
                                    if (item.Type == 4) {   // 0 - double; 1- int; 2- bool; 3- array
                                        var elmValueL = tmp as ElmValue_l;
                                        
                                        if (elmValueL != null)
                                        {
                                            propertyGrid.Item[propertyGrid.Item.Count - 1].CustomTypeConverter =
                                                new PostTypeConverter(item.Id, item.Name);
                                        }
                                    }
                                }
                                else
                                {
                                    tmp = item;
                                    propertyGrid.Item.Add(item.Name + s, ref tmp, "Value", item.ReadOnly, item.Category,
                                        item.Description, true);
                                    propertyGrid.Item[propertyGrid.Item.Count - 1].IsBrowsable = true;
                                    propertyGrid.Item[propertyGrid.Item.Count - 1].BrowsableLabelStyle =
                                        BrowsableTypeConverter.LabelStyle.lsNormal;
                                }
                                //propertyGrid.Item[propertyGrid.Item.Count - 1].B
                                propertyGrid.Item[propertyGrid.Item.Count - 1].Attributes = new AttributeCollection();

                            }
                        }
                    }
                    propertyGrid.Parent.Visible = true;
                }
            }
        }


        private void utzGant1_ChangedFrames(object sender, UTZ_Frame e)
        {
            PanelFrameTool.Visible = false;
            var utzGant = sender as UtzGant;
            if (utzGant != null)
            {
                PanelFrameTool.Controls.Clear();
                var o = utzGant.GantElms[utzGant.SelectLine][utzGant.SelectFrame];
                elm_property_grid.SelectedObject = o;
                if (o == null)
                {
                    PanelFrameTool.Visible = false;
                }
                else
                {
                    var _question = new Question();
                    var _userevent = new UserEvents();
                    switch (o.Type)
                    {
                        case 'T':
                            {
                                if (o.Tag == null)
                                {
                                    o.Tag = _question;
                                }
                                TestSysUTZ testSysUTZ = new TestSysUTZ();
                                testSysUTZ.Dock = DockStyle.Fill;
                                testSysUTZ.Question = (o.Tag as Question);
                                PanelFrameTool.Controls.Add(testSysUTZ);
                                PanelFrameTool.Visible = true;
                                break;
                            }
                        case 'U':
                            {
                                if (o.Tag == null)
                                {
                                    o.Tag = _userevent;
                                }
                                EventsControls eventsControls = new EventsControls();
                                eventsControls.Dock = DockStyle.Fill;
                                eventsControls.userEvents = (o.Tag as UserEvents);
                                PanelFrameTool.Controls.Add(eventsControls);
                                PanelFrameTool.Visible = true;
                                break;
                            }
                    }

                    //=================================================================
                    elementViewer1.IdUtz = _utz.ID;
                    elementViewer1.IdLayer = utzGant.GantElms[utzGant.SelectLine].Id;
                    elementViewer1.IdFrames = utzGant.SelectFrame;
                }
            }
        }


        private void User_Click(object sender, EventArgs e)
        {
            Form _userForm = new UserGroup();
            _userForm.StartPosition = FormStartPosition.CenterParent;
            _userForm.ShowDialog();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void elm_property_grid_Paint(object sender, PaintEventArgs e)
        {
            
            //elm_property_grid.ViewBackColor = Color.Chartreuse;
        }

        private void elementViewer1_MouseMove(object sender, MouseEventArgs e)
        {
            this.Text = e.X.ToString() + " : " + e.Y.ToString();
        }

        private void utzGant1_Load(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {

        }

        private void сбросToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            //MessageBox.Show(propertyGridEx1.SelectedObject.ToString());
        }

        private void elm_property_grid_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
        {
            //e.NewSelection.GridItems
            //MessageBox.Show(sender.ToString());
        }

        private void elm_property_grid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            //MessageBox.Show(s.ToString());
        }
    }
}
