﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataProviderLib.DateBase;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace UTZ_Wizard
{
    public partial class AddNTD : Form
    {
        Ntd _ntd = new Ntd();
        public AddNTD()
        {
            InitializeComponent();
            FilePath.DataBindings.Add(new Binding("Text", _ntd, "Path"));
            NtdName.DataBindings.Add(new Binding("Text", _ntd, "Name"));
            NtdDescription.DataBindings.Add(new Binding("Text", _ntd, "Description"));
            //


        }

        private void buttonFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.ShowDialog();
            if (openFile.FileName == "") return;
            if (_ntd.Name.Trim().Length == 0)
            {
                _ntd.File = openFile.SafeFileName;
                _ntd.Name = openFile.SafeFileName;
            }
            _ntd.Path = openFile.FileName;
            if (_ntd.Description.Trim().Length == 0)
                _ntd.Description = "Дата создания: " + DateTime.Now.ToString() + "\r\nИмя файла: " + openFile.SafeFileName + "\r\nПуть к файлу: " + openFile.FileName;

        }

        private void buttonCansel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (_ntd.File.Trim().Length == 0)
            {
                MessageBox.Show("Необходимо выбрать файл");
                return;
            }
            if (_ntd.Name.Trim().Length == 0)
            {
                MessageBox.Show("Необходимо указать имя файла");
                return;
            }
            var Provider = new DataProvider();
            Provider.Db.Insert(_ntd);
            this.Close();
        }
    }
}
