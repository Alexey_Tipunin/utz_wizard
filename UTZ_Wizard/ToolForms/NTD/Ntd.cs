﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using DataProviderLib.DateBase;
using PetaPoco.DataAnnotations;
using UTZ_Wizard.Annotations;

namespace UTZ_Wizard
{
    [Serializable]
    [TableName("Ntd")]
    [PrimaryKey("id")]
    public class Ntd : INotifyPropertyChanged
    {

        private static BindingList<Ntd> allNtd = new BindingList<Ntd>();
        private int _id = 0;
        private string _name = string.Empty;
        private string _description = string.Empty;
        private string _file = string.Empty;
        private string _path = string.Empty;


        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }

        public string File
        {
            get
            {
                return _file;
            }

            set
            {
                _file = value;
                OnPropertyChanged();
            }
        }

        public string Path
        {
            get
            {
                return _path;
            }

            set
            {
                _path = value;
                OnPropertyChanged();
            }
        }

        [Column("id")]
        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }
        [Ignore]
        public static BindingList<Ntd> AllNtd
        {
            get
            {
                var Provider = new DataProvider();
                foreach (var a in Provider.Db.Query<Ntd>("SELECT * FROM Ntd"))
                {
                    var o = allNtd.FirstOrDefault(x => x.Id == a.Id);
                    if (o == null)
                        allNtd.Add(a);
                    allNtd.OrderBy(x => x.Name);
                }
                return allNtd;
            }

            set
            {
                allNtd = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged( string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}