﻿namespace UTZ_Wizard
{
    partial class CreateUTZ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.UTZ_name = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.UTZ_Description = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ntd_lpanel = new System.Windows.Forms.Panel();
            this.NTD_FullList = new System.Windows.Forms.ListBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.ntd_cpanel = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.ntd_rpanel = new System.Windows.Forms.Panel();
            this.NTD_List = new System.Windows.Forms.ListBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textDescription = new System.Windows.Forms.TextBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonCreateUTZ = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.ntd_lpanel.SuspendLayout();
            this.ntd_cpanel.SuspendLayout();
            this.ntd_rpanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.UTZ_name);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox1.Size = new System.Drawing.Size(753, 55);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Наименование УТЗ";
            // 
            // UTZ_name
            // 
            this.UTZ_name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UTZ_name.Location = new System.Drawing.Point(10, 23);
            this.UTZ_name.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.UTZ_name.Name = "UTZ_name";
            this.UTZ_name.Size = new System.Drawing.Size(733, 20);
            this.UTZ_name.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.UTZ_Description);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 55);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox2.Size = new System.Drawing.Size(753, 182);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Описание и цели УТЗ";
            // 
            // UTZ_Description
            // 
            this.UTZ_Description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UTZ_Description.Location = new System.Drawing.Point(10, 23);
            this.UTZ_Description.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.UTZ_Description.Multiline = true;
            this.UTZ_Description.Name = "UTZ_Description";
            this.UTZ_Description.Size = new System.Drawing.Size(733, 149);
            this.UTZ_Description.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ntd_lpanel);
            this.groupBox3.Controls.Add(this.ntd_cpanel);
            this.groupBox3.Controls.Add(this.ntd_rpanel);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 237);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox3.Size = new System.Drawing.Size(753, 348);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "НТД и УММ";
            // 
            // ntd_lpanel
            // 
            this.ntd_lpanel.Controls.Add(this.NTD_FullList);
            this.ntd_lpanel.Controls.Add(this.textBox4);
            this.ntd_lpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ntd_lpanel.Location = new System.Drawing.Point(405, 23);
            this.ntd_lpanel.Name = "ntd_lpanel";
            this.ntd_lpanel.Size = new System.Drawing.Size(338, 315);
            this.ntd_lpanel.TabIndex = 2;
            // 
            // NTD_FullList
            // 
            this.NTD_FullList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NTD_FullList.FormattingEnabled = true;
            this.NTD_FullList.Location = new System.Drawing.Point(0, 20);
            this.NTD_FullList.Name = "NTD_FullList";
            this.NTD_FullList.Size = new System.Drawing.Size(338, 295);
            this.NTD_FullList.TabIndex = 3;
            this.NTD_FullList.SelectedIndexChanged += new System.EventHandler(this.NTD_FullList_SelectedIndexChanged);
            // 
            // textBox4
            // 
            this.textBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox4.Location = new System.Drawing.Point(0, 0);
            this.textBox4.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(338, 20);
            this.textBox4.TabIndex = 1;
            // 
            // ntd_cpanel
            // 
            this.ntd_cpanel.Controls.Add(this.button4);
            this.ntd_cpanel.Controls.Add(this.button5);
            this.ntd_cpanel.Controls.Add(this.button3);
            this.ntd_cpanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ntd_cpanel.Location = new System.Drawing.Point(330, 23);
            this.ntd_cpanel.Name = "ntd_cpanel";
            this.ntd_cpanel.Padding = new System.Windows.Forms.Padding(6, 80, 6, 6);
            this.ntd_cpanel.Size = new System.Drawing.Size(75, 315);
            this.ntd_cpanel.TabIndex = 1;
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.Location = new System.Drawing.Point(6, 126);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(63, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "<--";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.Location = new System.Drawing.Point(6, 103);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(63, 23);
            this.button5.TabIndex = 2;
            this.button5.Text = "Добавить";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.Location = new System.Drawing.Point(6, 80);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(63, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "-->";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ntd_rpanel
            // 
            this.ntd_rpanel.Controls.Add(this.NTD_List);
            this.ntd_rpanel.Controls.Add(this.textBox3);
            this.ntd_rpanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ntd_rpanel.Location = new System.Drawing.Point(10, 23);
            this.ntd_rpanel.Name = "ntd_rpanel";
            this.ntd_rpanel.Size = new System.Drawing.Size(320, 315);
            this.ntd_rpanel.TabIndex = 3;
            // 
            // NTD_List
            // 
            this.NTD_List.DisplayMember = "Name";
            this.NTD_List.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NTD_List.FormattingEnabled = true;
            this.NTD_List.Location = new System.Drawing.Point(0, 20);
            this.NTD_List.Name = "NTD_List";
            this.NTD_List.Size = new System.Drawing.Size(320, 295);
            this.NTD_List.TabIndex = 2;
            this.NTD_List.ValueMember = "Name";
            this.NTD_List.SelectedIndexChanged += new System.EventHandler(this.NTD_List_SelectedIndexChanged);
            // 
            // textBox3
            // 
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox3.Location = new System.Drawing.Point(0, 0);
            this.textBox3.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(320, 20);
            this.textBox3.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 585);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(753, 55);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textDescription);
            this.panel2.Controls.Add(this.linkLabel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(5);
            this.panel2.Size = new System.Drawing.Size(586, 55);
            this.panel2.TabIndex = 5;
            // 
            // textDescription
            // 
            this.textDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textDescription.Location = new System.Drawing.Point(5, 19);
            this.textDescription.Margin = new System.Windows.Forms.Padding(5);
            this.textDescription.Multiline = true;
            this.textDescription.Name = "textDescription";
            this.textDescription.ReadOnly = true;
            this.textDescription.Size = new System.Drawing.Size(576, 31);
            this.textDescription.TabIndex = 3;
            this.textDescription.Text = " wqeqwe qw eqwe qwe qwe qwewq ";
            // 
            // linkLabel1
            // 
            this.linkLabel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.linkLabel1.Location = new System.Drawing.Point(5, 5);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(576, 14);
            this.linkLabel1.TabIndex = 2;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "linkLabel1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonCancel);
            this.panel3.Controls.Add(this.buttonCreateUTZ);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(586, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(167, 55);
            this.panel3.TabIndex = 6;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(89, 14);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonCreateUTZ
            // 
            this.buttonCreateUTZ.Location = new System.Drawing.Point(6, 14);
            this.buttonCreateUTZ.Name = "buttonCreateUTZ";
            this.buttonCreateUTZ.Size = new System.Drawing.Size(75, 23);
            this.buttonCreateUTZ.TabIndex = 1;
            this.buttonCreateUTZ.Text = "Создать";
            this.buttonCreateUTZ.UseVisualStyleBackColor = true;
            this.buttonCreateUTZ.Click += new System.EventHandler(this.buttonCreateUTZ_Click);
            // 
            // CreateUTZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(753, 640);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "CreateUTZ";
            this.Text = "CreateUTZ";
            this.Load += new System.EventHandler(this.CreateUTZ_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ntd_lpanel.ResumeLayout(false);
            this.ntd_lpanel.PerformLayout();
            this.ntd_cpanel.ResumeLayout(false);
            this.ntd_rpanel.ResumeLayout(false);
            this.ntd_rpanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox UTZ_name;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox UTZ_Description;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel ntd_lpanel;
        private System.Windows.Forms.Panel ntd_cpanel;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel ntd_rpanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonCreateUTZ;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ListBox NTD_FullList;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.ListBox NTD_List;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textDescription;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Panel panel3;
    }
}