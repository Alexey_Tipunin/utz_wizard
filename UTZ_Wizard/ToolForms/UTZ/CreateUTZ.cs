﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataProviderLib.DateBase;
using Gant;

//using petapoco

namespace UTZ_Wizard
{
    public partial class CreateUTZ : Form
    {
        public CreateUTZ()
        {
            InitializeComponent();
        }

        private void buttonCreateUTZ_Click(object sender, EventArgs e)
        {
            var Provider = new DataProvider();
            UTZ _utz = new UTZ();
            Provider.Db.Insert(_utz);
            GantElm Layer = new GantElm();
            _utz.LayerUtz.Add(Layer);

            this.Close();
        }

        private void CreateUTZ_Load(object sender, EventArgs e)
        {
            UTZ _utz = new UTZ();
            UTZ_name.DataBindings.Add(new Binding("Text", _utz, "Name"));
            UTZ_Description.DataBindings.Add(new Binding("Text", _utz, "Description"));
            //NTD_List.DataBindings.Add(new Binding("Text", _utz, "NtdUtz"))
            NTD_List.DataSource = _utz.NtdUtz;
            NTD_List.ValueMember = "Id";
            NTD_List.DisplayMember = "Name";
            NTD_FullList.DataSource = Ntd.AllNtd;
            NTD_FullList.ValueMember = "Id";
            NTD_FullList.DisplayMember = "Name";
            //linkLabel1.DataBindings.Add(new Binding("Text", NTD_List.SelectedItems, "Name"));

        }

        private void NTD_List_SelectedIndexChanged(object sender, EventArgs e)
        {
            var  ntd  = NTD_List.SelectedItem as Ntd;
            if (ntd != null)
            {
                linkLabel1.Text =  ntd .File;
                textDescription.Text = ntd.Description;
            }
        }

        private void NTD_FullList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ntd = NTD_FullList.SelectedItem as Ntd;
            if (ntd != null)
            {
                linkLabel1.Text = ntd.File;
                textDescription.Text = ntd.Description;
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            var ntd = NTD_List.SelectedItem as Ntd;
            UTZ utz = new UTZ();
            if (utz != null && ntd != null) { 
                utz.NtdUtz.Remove(ntd);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var ntd = NTD_FullList.SelectedItem as Ntd;
            UTZ utz = new UTZ();
            if (utz != null && ntd != null)
            {
                if (!utz.NtdUtz.Contains(ntd))
                utz.NtdUtz.Add(ntd);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
           var addNTD = new AddNTD();
            addNTD.ShowDialog();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
