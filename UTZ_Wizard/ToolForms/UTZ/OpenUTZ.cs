﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataProviderLib.DateBase;
using PetaPoco.DataAnnotations;


namespace UTZ_Wizard
{
    public partial class OpenUTZ : Form
    {
        private static DataProvider Provider = new DataProvider();
        BindingList<UtzList> utzList = new BindingList<UtzList>(); 
        public OpenUTZ()
        {
            InitializeComponent();
        }


        private void OpenUTZ_Shown(object sender, EventArgs e)
        {
            try
            {
                utzList.Clear();

                foreach (var a in Provider.Db.Query<UtzList>("SELECT * FROM Utz order by Title"))
                {
                    utzList.Add(a);
                }
                var SelectUTZ = UTZ_List.SelectedItem as UtzList;
                if (SelectUTZ != null)
                {
                    textDescription.Text = SelectUTZ.Description;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.InnerException.Message);
                //throw;
            }

        }

        private void OpenUTZ_Load(object sender, EventArgs e)
        {
            UTZ_List.DataSource = utzList;
            UTZ_List.ValueMember = "Id";
            UTZ_List.DisplayMember = "Name";
        }

        private void UTZ_List_SelectedIndexChanged(object sender, EventArgs e)
        {
            var SelectUTZ = UTZ_List.SelectedItem as UtzList;
            if (SelectUTZ != null)
            {
                textDescription.Text = SelectUTZ.Description;
            }
        }

        private void button_Ok_Click(object sender, EventArgs e)
        {
            var SelectUTZ = UTZ_List.SelectedItem as UtzList;
            if (SelectUTZ != null)
            {
                UTZ _utz = new UTZ();
                _utz.ID = SelectUTZ.Id;
                _utz.Name = SelectUTZ.Name;
                _utz.Description = SelectUTZ.Description;
            }
            Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UTZ_List_DoubleClick(object sender, EventArgs e)
        {
            var SelectUTZ = UTZ_List.SelectedItem as UtzList;
            if (SelectUTZ != null)
            {
                UTZ _utz = new UTZ();
                _utz.ID = SelectUTZ.Id;
                _utz.Name = SelectUTZ.Name;
                _utz.Description = SelectUTZ.Description;
            }
            Close();
        }
    }

    public class UtzList
    {
        private int id = 0;
        private string name = String.Empty;
        private string description = String.Empty;
        [Column("id")]
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
        [Column("Title")]
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        [Column("Description")]
        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }
    }
}
