﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataProviderLib.DateBase;

namespace UTZ_Wizard.ToolForms.User
{
    public partial class AddUser : Form
    {
        private static UserDB tmpUserBD = new UserDB();
        public static string GetPass(int x)
        {
            string abc = "qwertyuiopasdfghjklzxcvbnm";
            abc += "!_@$&%";
            abc += "1234567890";
            abc += "QWERTYUIOPASDFGHJKLZXCVBNM";
            string pass = "";
            Random rnd = new Random();
            int lng = abc.Length;
            for (int i = 0; i < x; i++)
                pass += abc[rnd.Next(lng)];
            return pass;
        }
        public AddUser()
        {
            InitializeComponent();
        }

        private void PassGen_Click(object sender, EventArgs e)
        {
            tmpUserBD.Password = GetPass(8);

        }

        private void AddUser_Load(object sender, EventArgs e)
        {
            if (Tag == null)
                tmpUserBD = new UserDB();
            else
                tmpUserBD = (Tag as UserDB);
            Tag = null;
            textName.DataBindings.Add(new Binding("Text", tmpUserBD, "Name"));
            textSurname.DataBindings.Add(new Binding("Text", tmpUserBD, "Surname"));
            textPtronymic.DataBindings.Add(new Binding("Text", tmpUserBD, "Ptronymic"));
            textPassword.DataBindings.Add(new Binding("Text", tmpUserBD, "Password"));
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            var Provider = new DataProvider();
            Provider.Db.Insert("Student", "id", tmpUserBD);
            //_group.Add(tmpUserBD);
            //((sender as Button).Tag as Form).Close();
            this.Close();
            Tag = tmpUserBD;
        }
    }
}
