﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using DataProviderLib.DateBase;
using PetaPoco.DataAnnotations;
using UTZ_Wizard.Annotations;
using UTZ_Wizard.ToolForms.User;

namespace UTZ_Wizard
{
    public partial class UserGroup : Form
    {
        static GroupDB tmpGroupDbgroup = new GroupDB();
        BindingList<GroupDB> _group = new BindingList<GroupDB>();
        BindingList<UserDB> _user = new BindingList<UserDB>();
        public UserGroup()
        {

            InitializeComponent();
        }


        private void UserGroup_Load(object sender, EventArgs e)
        {
            GroupList.DataSource = _group;
            GroupList.ValueMember = "Id";
            GroupList.DisplayMember = "Name";
            UserList.DataSource = _user;
            UserList.ValueMember = "Id";
            UserList.DisplayMember = "Title";
            //=
            _group.Clear();
            var Provider = new DataProvider();
            foreach (var a in Provider.Db.Query<GroupDB>("SELECT * FROM `Group`"))
            {
                _group.Add(a);
            }
            if (GroupList.Items.Count>0)
            {
                var Provider2 = new DataProvider();
                foreach (var a in Provider.Db.Query<UserDB>($"SELECT * FROM `Student` WHERE idGroup = '{GroupList.SelectedValue}'"))
                {
                    _user.Add(a);
                }
            }
        }

        private void GroupList_SelectedIndexChanged(object sender, EventArgs e)
        {
            _user.Clear();
            var Provider = new DataProvider();
            foreach (var a in Provider.Db.Query<UserDB>($"SELECT * FROM `Student` WHERE idGroup = '{GroupList.SelectedValue}'"))
            {
                _user.Add(a);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form addGroup = new Form();
            addGroup.StartPosition = FormStartPosition.CenterParent;
            addGroup.Height = 106;
            addGroup.Width = 400;
            addGroup.FormBorderStyle = FormBorderStyle.FixedDialog;
            addGroup.MaximizeBox = false;
            addGroup.MinimizeBox = false;
            addGroup.Text = "Добавить группу";

            Panel _topPanel = new Panel();
            _topPanel.Dock = DockStyle.Fill;
            _topPanel.Height = 74;
            Panel _toolPanel = new Panel();
            _toolPanel.Dock = DockStyle.Bottom;
            _toolPanel.Height = 32;
            _toolPanel.Padding = new Padding(5); ;
            Button _okButton = new Button();
            _okButton.Text = "&Ok";
            _okButton.Click += OkButtonOnClick;
            _okButton.Tag = addGroup;
            _okButton.Dock = DockStyle.Right;
            TextBox _textBox = new TextBox();
            _textBox.Dock = DockStyle.Fill;
            _textBox.DataBindings.Add(new Binding("Text", tmpGroupDbgroup, "Name"));
            _topPanel.Controls.Add(_textBox);
            _topPanel.Padding = new Padding(10); ;
            //
            _toolPanel.Controls.Add(_okButton);
            addGroup.Controls.Add(_topPanel);
            addGroup.Controls.Add(_toolPanel);
            //
            addGroup.ShowDialog();
        }

        private void OkButtonOnClick(object sender, EventArgs eventArgs)
        {
            if (tmpGroupDbgroup.Name.Trim().Length == 0)
            {
                System.Windows.MessageBox.Show("Введите название группы");
                return;
            }
            else
            {
                var Provider = new DataProvider();
                Provider.Db.Insert("Group", "id", tmpGroupDbgroup);
                _group.Add(tmpGroupDbgroup);
                 ((sender as Button).Tag as Form).Close();
                
            }
        }

        private void AddUser_Click(object sender, EventArgs e)
        {
            var addUserForm = new AddUser();
            UserDB tmpUserDb = new UserDB();
            tmpUserDb.IdGroup = (int) GroupList.SelectedValue;
            addUserForm.Tag = tmpUserDb;
            addUserForm.StartPosition = FormStartPosition.CenterParent;
            addUserForm.ShowDialog();
            _user.Add(tmpUserDb);
        }
    }


    public class GroupDB : INotifyPropertyChanged
    {
        private int _id = 0;
        private string _name = string.Empty;
        [Column("id")]
        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }
        [Column("Title")]
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged( string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    public class UserDB : INotifyPropertyChanged
    {

        private int _id = 0;
        private int _idGroup = 0;
        private string _surname = string.Empty;
        private string _name = string.Empty;
        private string _ptronymic = string.Empty;
        private string _password = string.Empty;

        private string _title = string.Empty;

        [Column("id")]
        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public int IdGroup
        {
            get
            {
                return _idGroup;
            }

            set
            {
                _idGroup = value;
                OnPropertyChanged();
            }
        }

        public string Surname
        {
            get
            {
                return _surname;
            }

            set
            {
                _surname = value;
                OnPropertyChanged();
            }
        }

        public string Ptronymic
        {
            get
            {
                return _ptronymic;
            }

            set
            {
                _ptronymic = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }

            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        [Ignore]
        public string Title
        {
            get
            {
                return _surname + " " + _name + " " + _ptronymic;
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged( string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}
