﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Documents;
using DataProviderLib.DateBase;
using ElmViewer;
using Gant;
using PetaPoco.DataAnnotations;
using UTZ_Wizard.Annotations;
using static UTZ_Wizard._UTZ;

namespace UTZ_Wizard
{
    [Serializable]
    [TableName("Utz")]
    [PrimaryKey("id")]
    public class UTZ : INotifyPropertyChanged
    {
        private List<GantElm> tmpGantElms = new List<GantElm>();
        private List<Ntd> tmpNtdBD = new List<Ntd>();
        private static DataProvider Provider = new DataProvider();
        private static bool first = true;
        public UTZ()
        {
            if (first)
            {
                first = false;
                _layerUtz.CollectionChanged += LayerUtzCollectionChangedMethod;
                _ntdUtz.ListChanged += NtdUtzOnListChanged;
                _elementUtz.CollectionChanged += ElementUtzOnCollectionChanged;

            }

        }

        private void ElementUtzOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //switch (e.Action)
            //{
            //    case NotifyCollectionChangedAction.Add:
            //        {
            //            //
            //            _elementUtz.CollectionChanged += 
            //        }
            //}
            //throw new NotImplementedException();
        }

        private void NtdUtzOnListChanged(object sender, ListChangedEventArgs listChangedEventArgs)
        {

            switch (listChangedEventArgs.ListChangedType)
            {
                case ListChangedType.ItemAdded:
                    {
                        try
                        {
                            NtdBD d = new NtdBD();
                            d.Id_Utz = ID;
                            d.Id_Ntd = _ntdUtz[listChangedEventArgs.NewIndex].Id;
                            Provider.Db.Insert("UtzNtd", "id_Utz,id_Ntd", d);
                        }
                        catch (Exception)
                        {//throw;
                        }
                        break;
                    }
                case ListChangedType.ItemDeleted:
                    {
                        Provider.Db.Execute("DELETE FROM UtzNtd WHERE id_Utz=@0 AND id_Ntd=@1", ID, tmpNtdBD[listChangedEventArgs.NewIndex].Id);
                        break;
                    }
            }
            tmpNtdBD = _ntdUtz.ToList();
        }


        private void LayerUtzCollectionChangedMethod(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        LayersDB d = new LayersDB();
                        d.Order = e.NewStartingIndex;
                        d.Id_Utz = ID;
                        d.Name = _layerUtz[e.NewStartingIndex].Name;
                        d.Color = _layerUtz[e.NewStartingIndex].Color.ToArgb();
                        if (!(_layerUtz[e.NewStartingIndex].Id > 0))
                        if (!(_layerUtz[e.NewStartingIndex].Id > 0))
                        {
                            Provider.Db.Insert("Layers", "id", d);
                            _layerUtz[e.NewStartingIndex].Id = d.Id;
                        }
                        _layerUtz[e.NewStartingIndex].PropertyChanged += OnPropertyChanged;
                        break;
                    }
                case NotifyCollectionChangedAction.Remove:
                    {
                        Provider.Db.Delete("Layers", "id", tmpGantElms[e.OldStartingIndex]);
                        break;
                    }
            }
            tmpGantElms = _layerUtz.ToList();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            LayersDB d = new LayersDB();
            d.Id = (sender as GantElm).Id;
            d.Id_Utz = ID;
            d.Name = (sender as GantElm).Name;
            d.Color = (sender as GantElm).Color.ToArgb();
            Provider.Db.Update("Layers", "id", d);
            //throw new NotImplementedException();
        }

        [Column("id")]
        public int ID
        {
            get
            {
                return _id;
            }

            set
            {
#region Load_Layer_Table
                _id = value;
                _UTZ._layerUtz.Clear();
                var Provider = new DataProvider();
                foreach (var a in Provider.Db.Query<LayersDB>("SELECT * FROM Layers WHERE id_Utz = @0 Order by `Order`", _UTZ._id))
                {
                        var o = new GantElm();
                        o.Name = a.Name;
                        o.Id = a.Id;
                        o.Color = Color.FromArgb(a.Color);
                        _UTZ._layerUtz.Add(o);
                }
#endregion
//============================================
#region Load_Ntd_Table
                _id = value;
                _UTZ._ntdUtz.Clear();
                foreach (var a in Provider.Db.Query<Ntd>("SELECT Ntd.* FROM UtzNtd INNER JOIN Ntd ON Ntd.id = UtzNtd.id_Ntd AND UtzNtd.id_Utz = @0", _UTZ._id))
                {
                    _ntdUtz.Add(a);
                }
#endregion
#region Load_ElementUtz_Table
                _id = value;
                _UTZ._ntdUtz.Clear();
                foreach (var a in Provider.Db.Query<ElementUtz>("SELECT * FROM Element ")) // WHERE Element.`Type` <> 115
                {
                    //a.X1 = a.Id * 32;
                    //a.Y1 = a.Id * 32;
                    //a.X2 = a.Id * 32 + 100;
                    //a.Y2 = a.Id * 32;
                    _elementUtz.Add(a);
                }
#endregion
                OnPropertyChanged();
            }
        }
        [Column("Title")]
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }
        [Column("Description")]
        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }
        [Ignore]
        public ObservableCollection<ElementUtz> ElementUtz
        {
            get
            {
                return _elementUtz;
            }

            set
            {
                _elementUtz = value;
                OnPropertyChanged();
            }
        }
        [Ignore]
        public ObservableCollection<GantElm> LayerUtz
        {
            get
            {
                return _layerUtz;
            }

            set
            {
                _layerUtz = value;
                //OnPropertyChanged();
            }
        }
        [Ignore]
        public BindingList<Ntd> NtdUtz
        {
            get
            {
                return _ntdUtz;
            }

            set
            {
                _ntdUtz = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged( string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private class LayersDB
        {
            private int _id;
            private string _name;
            private int _color;
            private int id_Utz;
            private int _order;

            public int Id
            {
                get
                {
                    return _id;
                }

                set
                {
                    _id = value;
                }
            }

            public string Name
            {
                get
                {
                    return _name;
                }

                set
                {
                    _name = value;
                }
            }

            public int Color
            {
                get
                {
                    return _color;
                }

                set
                {
                    _color = value;
                }
            }

            public int Id_Utz
            {
                get
                {
                    return id_Utz;
                }

                set
                {
                    id_Utz = value;
                }
            }

            public int Order
            {
                get
                {
                    return _order;
                }

                set
                {
                    _order = value;
                }
            }
        }

        private class NtdBD
        {
            private int id_Utz;
            private int id_Ntd;

            public int Id_Utz
            {
                get
                {
                    return id_Utz;
                }

                set
                {
                    id_Utz = value;
                }
            }

            public int Id_Ntd
            {
                get
                {
                    return id_Ntd;
                }

                set
                {
                    id_Ntd = value;
                }
            }
        }
    }

    public static class _UTZ
    {
        public static int _id = 0;
        public static string _name = String.Empty;
        public static string _description = String.Empty;
        public static ObservableCollection<ElementUtz> _elementUtz = new ObservableCollection<ElementUtz>();
        public static ObservableCollection<GantElm> _layerUtz = new ObservableCollection<GantElm>();
        public static BindingList<Ntd> _ntdUtz = new BindingList<Ntd>();





    }
}