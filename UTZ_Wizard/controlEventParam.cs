﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataProviderLib.DateBase;
using UTZ_Wizard;

namespace TestSystem
{
    public partial class controlEventParam : UserControl
    {
        BindingList<elmUTZ> elemList = new BindingList<elmUTZ>();
        BindingList<proertyElm> elemProertyList = new BindingList<proertyElm>();
        private eventParam _eventParam;

        public eventParam EventParam
        {
            get
            {
                return _eventParam;
            }

            set
            {
                _eventParam = value;
                //cb_elem.SelectedValue = _eventParam.Id_elm;
                //cb_param.SelectedValue = _eventParam.Id_param;
            }
        }

        public controlEventParam()
        {
            InitializeComponent();
            elemList.Clear();
            DataProvider Provider = new DataProvider();
            foreach (var a in Provider.Db.Query<elmUTZ>("SELECT * FROM training.Element where Element.`Type` <> 112 order by Element.`Type`, Element.Name"))
            {
                elemList.Add(a);
            }
            //cb_elem.SelectedIndex = -1;
            //cb_param.SelectedIndex = -1;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private bool init_flag = false;
        private void controlEventParam_Load(object sender, EventArgs e)
        {
            cb_elem.DataSource = elemList;
            cb_elem.ValueMember = "Elm_id";
            cb_elem.DisplayMember = "Name";
            cb_param.DataSource = elemProertyList;
            cb_param.ValueMember = "Id_param";
            cb_param.DisplayMember = "Name";
            cb_elem.SelectedValue = _eventParam.Id_elm;
            cb_param.SelectedValue = _eventParam.Id_param;
            init_flag = true;
        }



        private proertyElm tmp;
        private void cb_param_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            p_val.Controls.Clear();
            tmp = (cb_param.SelectedItem as proertyElm);
            if (tmp == null)
            {
                //button1.Enabled = false;
                return;
            }
            //button1.Enabled = true;
            Control ValControl;
            if (tmp.Type == 2)
            {
                ValControl = new CheckBox();
                ValControl.DataBindings.Add("Checked", _eventParam, "Value", true);
            }
            else
            {
                ValControl = new TextBox();
                TextBox noze = new TextBox();
                noze.Dock = DockStyle.Right;
                noze.Width = 50;

                noze.DataBindings.Add("Text", _eventParam, "Noze", true);
                p_val.Controls.Add(noze);
                ValControl.DataBindings.Add("Text", _eventParam, "Value", true);
            }
            //===================================
            if (init_flag)
                _eventParam.Id_param = tmp.Id_param;
            //cb_param.SelectedValue = _eventParam.Id_param;
            //_eventParam.Value = 0;
            //_eventParam.Noze = 0;
            ValControl.Dock = DockStyle.Fill;
            p_val.Controls.Add(ValControl);
        }

        private void cb_elem_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            elemProertyList.Clear();
            var Provider = new DataProvider();
            if (cb_elem.SelectedValue != null)
            {
                elmUTZ tmp = (cb_elem.SelectedItem as elmUTZ);
                if (tmp != null)
                {
                    foreach (var a in Provider.Db.Query<proertyElm>($"SELECT * FROM `Parameter` WHERE Id_ElementType = '{tmp.Elm_type} order by `Group`, `Title`'"))
                    {
                        elemProertyList.Add(a);
                    }
                    if (init_flag)
                        _eventParam.Id_elm = tmp.Elm_id;
                }
            }
            //cb_param.SelectedValue = _eventParam.Id_param;
            //cb_param.SelectedIndex = -1;
            //Ok.Enabled = false;
            //upassword.Enabled = false;
            //upassword.Text = "";
            
        }

    }
}
